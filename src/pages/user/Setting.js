import React, { Component } from 'react';
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import UserDetailsForm from '../../components/user-dashboard/UserDetailsForm';
import UserTeacherAccess from '../../components/user-dashboard/UserTeacherAccess';
import UserCardDetails from '../../components/user-dashboard/UserCardDetails';
import axios from 'axios';
import { connect } from 'react-redux';
import {Helmet} from "react-helmet";
import { apiRoute, getApiHeader } from '../../utils/helpers';
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import { getLocalStorageAuth, removeLocalStorageAuth } from '../../utils/helpers';
import Recaptcha from 'react-google-invisible-recaptcha';


class Setting extends Component {
    constructor(props) {
        super(props);
        this.deleteModal = React.createRef();
        this.state = {
            userId:'',
            userFirstName:'',
            userLastName:'',  
            alert: false,
            alertType: '',
            alertMsg: '',
            userType:'1',
            showTeacherAccessError:false,
        }
      };

      componentWillMount() {
        window.scrollTo(0, 0);
        const auth = getLocalStorageAuth();
        const userDetails = auth.userDetails;
        if(auth){
            const userId = userDetails.id;
            this.setState({ userId: userId, userLastName:userDetails.last_name,userFirstName: userDetails.first_name, userType:userDetails.user_type});
        }
        if(this.props.location.state){
            this.setState({ showTeacherAccessError: true});
        }
        }

      deleteAccount = () => {
        const requestOptions = {
          headers: getApiHeader()
        };
        this.deleteModal.current.click();
        this.props.setLoadingTrue();
        axios.get(apiRoute('user-dashboard/delete-user-account/' + this.state.userId), requestOptions)
          .then(res => {
            window.scrollTo(0, 0);
            removeLocalStorageAuth();
            this.setState({ alert: true, alertType: 'success', alertMsg: res.data.message, subscriptionStatus: 0 });
            this.props.clearAllAlerts();
            setTimeout(() => {
                this.props.history.push('/');
            }, 2000);
          })
          .catch(error => {
            this.setState({ alert: true, alertType: 'error', alertMsg: 'Something went wrong please try again.' });
            this.props.clearAllAlerts();
          });
      }

    render() {
        const { alert, alertType, alertMsg } = this.state;
        return (
            <>
             <Helmet>
                <meta charSet="utf-8" />
                <title>Setting.</title>
            </Helmet>
            <Recaptcha
			ref={ ref => this.recaptcha = ref }
			sitekey='6LcXXb0ZAAAAAPKkrgEa8LPjOazq4InauFR3azbD'
			onResolved={ () => console.log( 'Human detected.' ) } />
            <div className="t3-wrapper">
                <Header />
                <main class="admin-content">
                    <section class="sec">
                        <div class="container">
                            <h4>Account Settings</h4>
                            {this.state.showTeacherAccessError &&
                                <div className="alert alert-danger col-sm-8" role="alert">
                                <p>You not have authorisation to access this page</p>
                                </div>}
                            {alert && alertType === 'error' &&
                                <div className="alert alert-danger" role="alert">
                                {alertMsg}
                                </div>}
                            {alert && alertType === 'success' &&
                                <div className="alert alert-success" role="alert">
                                {alertMsg}
                                </div>} 
                            <div class="card card-panel">
                                {/* <div class="text-right">
                                    <button class="btn btn-sm my-3" type="submit" name="action" data-toggle="modal" data-target="#delete-account">Delete my account</button>
                                </div> */}
                                <ul class="nav nav-tabs custom-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                                            aria-selected="true">My Profile</a>
                                    </li>
                                    {this.state.userType == '0' &&
                                    <>
                                     <li class="nav-item">
                                     <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment"
                                         aria-selected="false">Payment</a>
                                    </li>
                                    </>
                                    }
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
                                            aria-selected="false">Teacher Exclusive</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active pt-5" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                        <UserDetailsForm history={this.props.history} />
                                    </div>
                                    {this.state.userType == '0' &&
                                    <>
                                    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                                        <UserCardDetails history={this.props.history} />
                                    </div>
                                    </>
                                    }
                                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                        <UserTeacherAccess history={this.props.history} />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade show" id="delete-account" tabindex="-1" role="dialog" aria-labelledby="manage-methodTitle" aria-modal="true">
                            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                <h6 class="mb-3">Delete Sattva Account Permanently</h6>
                                <p>HI  {this.state.userFirstName}  {this.state.userLastName},</p>
                                <p>Deleting your account will have following effects:</p>
                                <ul>
                                    <li>Your current active subscription (if any)&nbsp;will be canceled.</li>
                                    <li>All your payment agreements will be void.</li>
                                    <li>Your payment methods (if any) will be detached from payment gateway.</li>
                                    <li>You will&nbsp;no longer be able to access the Sattva Connect platform.</li>
                                    <li>You will be immediately logged out from the Sattva Connect platform.</li>
                                </ul>          
                                <div class="text-right">
                                    <a onClick={this.deleteAccount} class="pop btn btn-sm red waves-effect waves-light" data-container="body" data-toggle="popover" data-placement="top" data-content="Delete anyway" data-original-title="" title=""><i class="fas fa-trash"></i></a>
                                    <button  ref={this.deleteModal} type="button" class="btn btn-sm waves-effect waves-light ml-2" data-dismiss="modal"><i class="fas fa-times"></i></button>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </section>
                </main>
                <Footer/>
                </div>
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    alert: state.alert,
  });
  
  export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(Setting);