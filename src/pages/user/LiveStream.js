import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import axios from 'axios';
import { apiRoute, getApiHeader, getLocalStorageAuth } from '../../utils/helpers';
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import VideoDetails from '../../components/user-dashboard/VideoDetails';
import {Helmet} from "react-helmet";
import ReactTooltip from "react-tooltip";

class LiveStream extends Component {

  constructor(props) {
    super(props);
    this.cancelModal = React.createRef();
    this.state = {
      streamVideos: [],
      userType: 0,
      liveStatus:1,
      teacherName:'',
      stopStream: true,
    }
  };

  componentWillMount() {
    window.scrollTo(0, 0);
    const auth = getLocalStorageAuth();
    if(auth){
    const userType = auth.userDetails.user_type;
    this.setState({ userType: userType });
    const requestOptions = {
      headers: getApiHeader()
    };
    this.props.setLoadingTrue();
    axios.get(apiRoute('user-dashboard/get-stream-videos'), requestOptions)
      .then(res => {
        this.setState({ streamVideos: res.data });
        this.props.clearAllAlerts();
      })
      .catch(error => {
        this.props.clearAllAlerts();
      });

      axios.get(apiRoute('get_opentok_token'),requestOptions)
			.then(res =>{
				this.setState({teacherName:res.data.name, liveStatus:res.data.status, sessionId: res.data.sessionId, });
      });
      
    }
    
  }

  stopStream = () => {
    this.cancelModal.current.click();
    const requestOptions = {
			headers: getApiHeader()
		};
		//Update steam status
		axios.get(apiRoute('update_stream_status/' + this.state.sessionId + '/' + 0), requestOptions)
			.then(res => {
				this.setState({teacherName:'', liveStatus:0, });
			});
  }

  render() {

    return (
      <>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>Live Stream.</title>
            </Helmet>
      <div className="t3-wrapper">
        <Header />
        <main className="admin-content">
          <section className="inner-banner" style={{ background: 'url(../images/anadlivestreambanner1.jpg)', backgroundSize: 'cover', minHeight: '500px' }}>
            <div className="container text-center text-white">
              <h1>Welcome to Sattva Connect <br />Live Broadcasting.</h1>
            </div>
          </section>
          <section className="sec sec-inabout">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <p>Sattva Connect has regular Live Stream events. Please see the schedule for upcoming live streams by pressing the CALENDAR button. To view an ongoing live stream, click the JOIN LIVE STREAM button.</p>
                  <p>All live streams are recorded and available on Sattva Connect shortly after the live stream event.</p>
                </div>
                <div className="col-md-12 text-center mt-5">
                  <div className="stream-btns">
                    {this.state.userType == 1 && this.state.liveStatus == 0 ?
                      <a className="btn btn-sm mr-2" href="/user-dashboard/go-live">Live Broadcast Yourself</a>
                      : null
                    }
                    {this.state.userType == 1 && this.state.liveStatus == 1 ?
                    <>
                      <Link  data-toggle="modal" data-target="#stopstream" data-html={true} data-for='custom-color-no-arrow' data-tip={'<h6>'+this.state.teacherName+' is Live</h6>'} className="btn btn-sm mr-2 ">Stop stream</Link>
                      <ReactTooltip id='custom-color-no-arrow' className='react-tooltip videoTooltip' delayHide={0}
                            textColor='#000' backgroundColor='#ffff' effect='solid'/>
                            </>
                      : null
                    }
                    <Link className="btn btn-sm mr-2" to="/user-dashboard/join-live">Join Live Stream</Link>
                    <Link className="btn btn-sm" to="/user-dashboard/upcoming-stream">Calendar <i className="fas fa-calendar-alt" /></Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section className="sec">
            <div className="container">
              <div className="class-block my-0 border-0">
                <h4>Recorded Live Streams</h4>
                <div className="row">
                {this.state.streamVideos.map((item, index) => {
                  return (
                    <VideoDetails item={item} key={item.id} />
                  );
                })}
                </div>
                {this.state.streamVideos.length > 0 ?
                  null
                  :
                  <div className="card-panel valign-wrapper grey lighten-4">
                    <div className="row">
                      <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                      <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                        <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                      </div>
                    </div>
                  </div>
                }
              </div>
            </div>
          </section>
        </main>
        <Footer />
      </div>
      <div className="modal fade" id="stopstream" tabindex="-1" role="dialog" aria-labelledby="transactionsTitle" aria-hidden="true">
          <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <h6 className="mb-3">Stop Stream</h6>
                <div className="table-responsive scroll-visible">
                 Are you sure you want to end the Live Stream of {this.state.teacherName}?
                </div>
               
                <div class="text-right mt-6">
                <button type="button" className="btn btn-sm mr-3" onClick={this.stopStream}>Yes</button>
                  <button type="button" ref={this.cancelModal} className="btn btn-sm" data-dismiss="modal"><i class="fas fa-times"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    )

  }
}

const mapStateToProps = (state) => ({
  alert: state.alert,
});
export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(LiveStream);