import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import OwlCarousel from 'react-owl-carousel';
import { apiRoute, getApiHeader } from '../../utils/helpers';
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import VideoDetails from '../../components/user-dashboard/VideoDetails';
import VideoDetailsMobile from '../../components/user-dashboard/VideoDetailsMobile';
import {Helmet} from "react-helmet";

class Explore extends Component {

    constructor(props) {
        super(props);
        this.state = {
            asanaLab: [],
            pranayamaLab: [],
            kriyaLab: [],
            mantraLab: [],
            newlyAdded: [],
            allStyles: [],
        }
    };

    componentDidMount() {

        const requestOptions = {
            headers: getApiHeader()
        };
        this.props.setLoadingTrue();
        axios.get(apiRoute('user-dashboard/get-style-videos/' + 4 + '/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                if (res.data) {
                    this.setState({ asanaLab: res.data.videos });
                }
                this.props.clearAllAlerts();
            })
            .catch(error => {
                this.props.clearAllAlerts();
            });

        axios.get(apiRoute('user-dashboard/get-style-videos/' + 5 + '/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                if (res.data) {
                    this.setState({ pranayamaLab: res.data.videos });
                }
            });

        axios.get(apiRoute('user-dashboard/get-style-videos/' + 7 + '/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                if (res.data) {
                    this.setState({ kriyaLab: res.data.videos });
                }
            });

        axios.get(apiRoute('user-dashboard/get-style-videos/' + 11 + '/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                if (res.data) {
                    this.setState({ mantraLab: res.data.videos });
                }
            });

        axios.get(apiRoute('user-dashboard/get-all-videos/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                if (res.data) {
                    this.setState({ newlyAdded: res.data.videos });
                }
            });

        axios.get(apiRoute('user-dashboard/get-all-videos-style'), requestOptions)
            .then(res => {
                if (res.data) {
                    this.setState({ allStyles: res.data });
                }
            });

        window.scrollTo(0, 0);
    }
    getShortWord = (str) => {
        var matches = str.match(/\b(\w)/g);
        var acronym = matches.join('');
        return acronym;
    }

    render() {
        return (
            <>
             <Helmet>
                <meta charSet="utf-8" />
                <title>Explore.</title>
            </Helmet>
            <div className="t3-wrapper">
                <Header />
                <main className="admin-content">
                    <section className="sec sec-explore pb-0">
                        <div className="container">
                            <h4 className="h4-style mb-md-5 mb-4">Browse By Style</h4>
                            <div className="row">
                                {this.state.allStyles.map((item, index) => {
                                    return (
                                        <div className="col-xl-3 col-lg-3 col-md-3 col-sm-6 user-list">
                                            <Link className="" to={{pathname:"/user-dashboard/search/", style:item.id}}>
                                                <div className="avatar-me-wrapper">
                                                    <span className="avatar-me">{this.getShortWord(item.type)}</span>
                                                    <strong className="user-name">{item.type}</strong>
                                                </div>
                                            </Link>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </section>
                    <section className="sec sec-style sec-desktop">
                        <div className="container">
                            <div className="class-block mt-0">
                                <h4 className="h4-style">Asana Lab</h4>
                                <div className="row">
                                    {this.state.asanaLab.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id}/>
                                        );
                                    })}
                                </div>
                                {this.state.asanaLab.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/asana-lab">View All</Link>
                                    </div>
                                    : null}
                                 {this.state.asanaLab.length == 0 ?    
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }

                            </div>
                            <div className="class-block">
                                <h4 className="h4-style">Pranayama Lab</h4>
                                <div className="row">
                                    {this.state.pranayamaLab.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id}/>
                                        );
                                    })}
                                </div>
                                {this.state.pranayamaLab.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/prayanama-lab">View All</Link>
                                    </div>
                                    : 
                                    null}
                                    {this.state.pranayamaLab.length == 0 ? 
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }

                            </div>
                            <div className="class-block">
                                <h4 className="h4-style">Kriya Lab</h4>
                                <div className="row">
                                    {this.state.kriyaLab.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id}/>
                                        );
                                    })}
                                </div>
                                {this.state.kriyaLab.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/kriya-lab">View All</Link>
                                    </div>
                                    : null }
                                     {this.state.kriyaLab.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                      : null
                                }

                            </div>
                            <div className="class-block">
                                <h4 className="h4-style">Mantra Lab</h4>
                                <div className="row">
                                    {this.state.mantraLab.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id}/>
                                        );
                                    })}
                                </div>
                                {this.state.mantraLab.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/mantra-lab">View All</Link>
                                    </div>
                                    : null }
                                    {this.state.mantraLab.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }

                            </div>
                            <div className="class-block">
                                <h4 className="h4-style">Newly Added Videos</h4>
                                <div className="row">
                                    {this.state.newlyAdded.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id}/>
                                        );
                                    })}
                                </div>
                                {this.state.newlyAdded.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/new-videos">View All</Link>
                                    </div>
                                    : null }
                                     {this.state.newlyAdded.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }

                            </div>
                        </div>
                    </section>
                    <section className="sec sec-mobile">
                        <div className="class-block mt-0">
                            <div className="container class-block-header">
                                <h4 className="h4-style">Asana Lab</h4>
                                {this.state.asanaLab.length > 1 ?
                                
                                    <Link className="btn btn-sm" to="/user-dashboard/asana-lab">View All</Link>
                              
                                : null }
                            </div>
                            
                            
                             {this.state.asanaLab.length == 0 ?   
                                     <div className="container"> 
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                </div></div>
                                : 
                                <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.asanaLab.map((item, index) => {
                                    return (
                                        <VideoDetailsMobile item={item} key={item.id}/>
                                    );
                                })}
                            </OwlCarousel>
                            }

                        </div>
                        <div className="class-block">
                            <div className="container class-block-header">
                            <h4 className="h4-style">Pranayama Lab</h4>
                            {this.state.pranayamaLab.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/prayanama-lab">View All</Link>
                                
                                : null }
                            </div>
                            
                           
                              {this.state.pranayamaLab.length == 0 ?   
                              <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                </div></div>
                                : 
                                <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.pranayamaLab.map((item, index) => {
                                    return (
                                        <VideoDetailsMobile item={item} key={item.id}/>
                                    );
                                })}
                            </OwlCarousel>
                            }

                        </div>
                        <div className="class-block">
                            <div className="container class-block-header">
                                <h4 className="h4-style">Kriya Lab</h4>
                                {this.state.kriyaLab.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/kriya-lab">View All</Link>
                                
                                : null }
                            </div>
                           
                                {this.state.kriyaLab.length == 0 ?
                                <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                </div></div>
                                 : 
                                 <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                 {this.state.kriyaLab.map((item, index) => {
                                     return (
                                         <VideoDetailsMobile item={item} key={item.id}/>
                                     );
                                 })}
                             </OwlCarousel>
                             
                            }

                        </div>
                        <div className="class-block">
                            <div className="container class-block-header">
                            <h4 className="h4-style">Mantra Lab</h4>
                            {this.state.mantraLab.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/mantra-lab">View All</Link>
                               
                                : null }
                            </div>
                                 {this.state.mantraLab.length == 0 ?
                                 <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div></div>
                                </div>
                                 : 
                                 <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.mantraLab.map((item, index) => {
                                    return (
                                        <VideoDetailsMobile item={item} key={item.id}/>
                                    );
                                })}
                            </OwlCarousel>
                            }

                        </div>
                        <div className="class-block">
                            <div className="container class-block-header">
                                <h4 className="h4-style">Newly Added Videos</h4>
                                {this.state.newlyAdded.length > 1 ?
                                        <Link className="btn btn-sm" to="/user-dashboard/new-videos">View All</Link>
                                    
                                    :  null }
                            </div>
                           
                               
                                     {this.state.newlyAdded.length == 0 ? 
                                     <div className="container">
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-md-0 mt-3 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div></div>
                                      :  
                                      <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                      {this.state.newlyAdded.map((item, index) => {
                                          return (
                                              <VideoDetailsMobile item={item} key={item.id}/>
                                          );
                                      })}
                                  </OwlCarousel>
                                }

                            </div>
                      
                    </section>
                </main>
                <Footer />
                                
            </div>
            </>
        )
    }
}

const mapStateToProps = (state) => ({
    alert: state.alert,
});

export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(Explore);