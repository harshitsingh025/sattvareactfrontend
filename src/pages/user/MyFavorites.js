import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {fetchMyFavoriteVideos} from '../../actions/videoAction';
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import VideoDetails from '../../components/user-dashboard/VideoDetails';
import { getUserId } from '../../utils/helpers';
import {Helmet} from "react-helmet";
import { InfiniteScroll } from 'react-simple-infinite-scroll';

class MyFavorites extends Component {
    constructor(props){
        super(props);
        this.state = {
            userId:'',
            limit:40,
        }
    };
    loadMore = () => {
		this.props.fetchMyFavoriteVideos(this.state.userId,this.state.limit, this.props.cursor,this.props.videos);
      } 

      componentDidMount() {
        window.scrollTo(0, 0);
        const userId = getUserId();
        this.setState({ userId : userId });
        this.props.fetchMyFavoriteVideos(userId,this.state.limit, 0,[]);
        }

    render() {
        return (
            <>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>My Favorites.</title>
            </Helmet>
            <div className="t3-wrapper">
                <Header/>
                <main className="admin-content">
                    <div className="sec pb-0">
                    <div className="container">
                        <div className="class-block m-0">
                             <div className="row">
                                <div className="col-md-6 col">
                                    <h4>My Favorites</h4>
                                </div>
                                <div className="col-md-6 col text-right">
                                    <Link className="btn btn-sm" to="/user-dashboard/me">Back</Link>
                                </div>
                            </div>
                        <InfiniteScroll
                            throttle={100}
                            threshold={300}
                            isLoading={alert.isloading}
                            hasMore={this.props.hasMore}
                            onLoadMore={this.loadMore}
                        >
                        <div className="row">
                       
                        {this.props.videos.map((item,index)=>{
                        return(
                            <VideoDetails item={item.video} key={item.id}/>
                            );
                            })}
                            
                            </div>
                            </InfiniteScroll>
                        </div>
                    </div>
                    </div>
                </main>
                <Footer/>
            </div>
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    videos: state.video.items,
    alert: state.alert,
    cursor:state.video.cursor,
    hasMore:state.video.hasMore,
});

export default connect(mapStateToProps, {fetchMyFavoriteVideos })(MyFavorites);
