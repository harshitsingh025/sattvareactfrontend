import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {fetchStyleVideos} from '../../actions/videoAction';
import {setLoadingTrue, clearAllAlerts} from '../../actions/userAction';
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import VideoDetails from '../../components/user-dashboard/VideoDetails';
import {Helmet} from "react-helmet";
import { InfiniteScroll } from 'react-simple-infinite-scroll';


class PranayamaLab extends Component {
    constructor(props){
        super(props);
        this.state = {
            styleId:5,
            limit:40,
        }
    };
    loadMore = () => {
		this.props.fetchStyleVideos(this.state.styleId,this.state.limit, this.props.cursor,this.props.videos);
      } 
      
    componentWillMount() {
        window.scrollTo(0, 0);
        this.props.fetchStyleVideos(this.state.styleId,this.state.limit, 0,[]);
        }

    render() {
        return (
            <>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>Pranayamalab.</title>
            </Helmet>
            <div className="t3-wrapper">
                <Header/>
                <main className="admin-content">
                    <section className="sec sec-style">
                    <div className="container">
                        <div className="class-block mt-0">
                        <div className="row">
                                <div className="col-md-6 col">
                        <h4>Pranayama Lab</h4>
                                </div>
                                <div className="col-md-6 col text-right">
                                    <Link className="btn btn-sm" to="/user-dashboard/explore">Back</Link>
                                </div>
                            </div>
                            <InfiniteScroll
                        throttle={100}
                        threshold={300}
                        isLoading={alert.isloading}
                        hasMore={this.props.hasMore}
                        onLoadMore={this.loadMore}
                        >
                        <div className="row">
                       
                        {this.props.videos.map((item,index)=>{
                        return(
                            <VideoDetails item={item} key={item.id}/>
                            );
                            })}
                            </div>
                            </InfiniteScroll>
                        </div>
                    </div>
                    </section>
                </main>
                <Footer/>
            </div>
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    videos: state.video.items,
    alert: state.alert,
    cursor:state.video.cursor,
    hasMore:state.video.hasMore,
});

export default connect(mapStateToProps, {fetchStyleVideos })(PranayamaLab);
