import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setLoadingTrue, clearAllAlerts } from '../../../actions/userAction';
import Chat from '../../../components/user-dashboard/live-stream/Chat';
import GoLive from '../../../components/user-dashboard/live-stream/GoLive';
import Header from '../../../components/user-dashboard/common/Header';
import Footer from '../../../components/user-dashboard/common/Footer';
import { getLocalStorageAuth } from '../../../utils/helpers';
import { Helmet } from "react-helmet";


class LiveStream extends Component {
    constructor(props) {
        super(props);
        const userData = getLocalStorageAuth();
        if (userData) {
            const firstName = userData.userDetails.first_name;
            const lastName = userData.userDetails.last_name;
            this.state = {
                first_name: firstName,
                last_name: lastName,
            }
        }
    };

    render() {
        return (
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Live Stream</title>
                    <meta http-equiv='cache-control' content='no-cache' />
                    <meta http-equiv='expires' content='0' />
                    <meta http-equiv='pragma' content='no-cache' />
                </Helmet>
                <div className="t3-wrapper">
                    <div className="broadcastHeader">
                        <Header className="d-sm-none d-block" />
                    </div>
                    <main className="admin-content">
                        <a href="/user-dashboard/live-stream" className="btn btn-toggle waves-effect waves-light"><i className="fas fa-arrow-left"></i></a>
                        <section className="sec-broadcast">
                            <div className="card-broadcast">
                                <div className="broadcast-box">
                                    <div className="broadcast-header">
                                        <h4 className="user-name mb-0">{this.state.first_name} {this.state.last_name} <span className="btn-success"></span>  Note - Please don't Close this window or Refresh this page.</h4>
                                        {/* <a className="btn btn-sm"><i className="far fa-trash-alt"></i> Clear</a> */}
                                    </div>
                                    <div className="row m-0">
                                        <GoLive />
                                        <Chat userType='1' />
                                    </div>
                                </div>
                            </div>
                        </section>
                        {/* <section className="sec sec-counter">
            <div className="container text-center">
                <h4>Next Live Streaming with Anandji</h4>
                <div className="countdown-container">                
                    <div className="to-date"><span id="to"></span></div>
                    <div className="countdown"></div>
                </div>
            </div>
            </section> */}
                    </main>
                    <div className="broadcastFooter">
                        <Footer />
                    </div>
                </div>
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    alert: state.alert,
});

export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(LiveStream);
