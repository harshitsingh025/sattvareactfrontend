import React, { Component } from 'react';
import { connect } from 'react-redux';
import {setLoadingTrue, clearAllAlerts} from '../../../actions/userAction';
import Chat from '../../../components/user-dashboard/live-stream/Chat';
import JoinLive from '../../../components/user-dashboard/live-stream/JoinLive';
import Header from '../../../components/user-dashboard/common/Header';
import Footer from '../../../components/user-dashboard/common/Footer';
import {Helmet} from "react-helmet";


class JoinStream extends Component {
    constructor(props){
        super(props);
    };
  
    render() {
        return (
            <>
             <Helmet>
                <meta charSet="utf-8" />
                <title>Live Stream</title>
				<meta http-equiv='cache-control' content='no-cache'/>
                <meta http-equiv='expires' content='0'/>
                <meta http-equiv='pragma' content='no-cache'/>
			</Helmet>
            <div className="t3-wrapper">
            <div className="broadcastHeader">
                <Header className="d-sm-none d-block" />
            </div>
            <main className="admin-content">
            <a href="/user-dashboard/live-stream" className="btn btn-toggle waves-effect waves-light"><i className="fas fa-arrow-left"></i></a>
            <section className="sec-broadcast">
                <div className="card-broadcast">
                        <div className="broadcast-box">
                            {/* <div className="broadcast-header">
                                <h4 className="user-name mb-0">Welcome to Sattva Connect Live Streaming, Harshit singh is Online</h4>
                            </div> */}
                            {/* <div className="broadcast-header">
                                <h4 className="user-name mb-0">Saraswati <span className="btn-success"></span></h4>
                                <a className="btn btn-sm"><i className="far fa-trash-alt"></i> Clear</a>
                            </div> */}
                            {/* <div className="row m-0"> */}
                                <JoinLive/>
                               {/* <Chat/>
                            </div> */}
                        </div>
                    </div>
            </section>
            {/* <section className="sec sec-counter">
            <div className="container text-center">
                <h4>Next Live Streaming with Anandji</h4>
                <div className="countdown-container">                
                    <div className="to-date"><span id="to"></span></div>
                    <div className="countdown"></div>
                </div>
            </div>
            </section> */}
            </main>  
            <div className="broadcastFooter">
                <Footer/>
            </div>
            </div>
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    alert: state.alert,
});

export default connect(mapStateToProps, {setLoadingTrue, clearAllAlerts })(JoinStream);
