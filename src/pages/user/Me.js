import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
import OwlCarousel from 'react-owl-carousel';
import { apiRoute, getApiHeader } from '../../utils/helpers';
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import { getUserId } from '../../utils/helpers';
import VideoDetails from '../../components/user-dashboard/VideoDetails';
import VideoDetailsMobile from '../../components/user-dashboard/VideoDetailsMobile';
import {Helmet} from "react-helmet";


class Me extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            myClasses: [],
            myFavorite: [],
            recentlyWatched: [],
            recommended: [],
            newlyAdded: [],
            alert: false,
            alertType: '',
            alertMsg: '',
        }
    };
    componentDidMount() {
        const userId = getUserId(this.props.history);
        this.setState({ userId: userId });
        const requestOptions = {
            headers: getApiHeader()
        };
        this.props.setLoadingTrue();
        axios.get(apiRoute('user-dashboard/get-favorite-videos/' + userId + '/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                this.setState({ myFavorite: res.data.videos });
                this.props.clearAllAlerts();
            })
            .catch(error => {
                this.props.clearAllAlerts();
            });

        axios.get(apiRoute('user-dashboard/get-classes-videos/' + userId + '/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                this.setState({ myClasses: res.data.videos });
            });

        axios.get(apiRoute('user-dashboard/get-recent-watched-videos/' + userId + '/' +  + 0 + '/' + 4), requestOptions)
            .then(res => {
                this.setState({ recentlyWatched: res.data.videos });
            });

        axios.get(apiRoute('user-dashboard/get-all-videos/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                this.setState({ newlyAdded: res.data.videos });
            });

        axios.get(apiRoute('user-dashboard/get-all-featured-videos/' + 0 + '/' + 4), requestOptions)
            .then(res => {
                this.setState({ recommended: res.data.videos });
            });

        window.scrollTo(0, 0);
    }
    render() {
        return (
            <>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>Me.</title>
            </Helmet>
            <div className="t3-wrapper">
                <Header />
                <main className="admin-content">
                    <section className="sec pb-0">
                        <div className="container">
                            <h4 className="h4-style">Welcome to Sattva Connect</h4>
                            <div className="card-panel">
                                <p>Namaste and welcome to your homepage. This is where you can create your own playlist entitled "My classes," add your favorite classes, see your recently watched videos, get our recommendations based on your interests and stay updated as to what new videos have been added since your last visit.</p>
                                <div className="waveWrapper waveAnimation">
                                    <div className="waveWrapperInner bgTop">
                                        <div className="wave waveTop" />
                                    </div>
                                    <div className="waveWrapperInner bgMiddle">
                                        <div className="wave waveMiddle" />
                                    </div>
                                    <div className="waveWrapperInner bgBottom">
                                        <div className="wave waveBottom" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="sec sec-desktop pb-0">
                        <div className="container">                        
                            <div className="class-block mt-0">
                                <h4>My Classes</h4>
                                <div className="row">
                                    {this.state.myClasses.map((item, index) => {
                                        return (
                                            <VideoDetails item={item.video} key={item.video.id} />
                                        );
                                    })}
                                </div>
                                {this.state.myClasses.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/my-classes">View All</Link>
                                    </div>
                                    : null }
                                    {this.state.myClasses.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">Tap the My Classes button and you’ll see your Classes videos here.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }
                            </div>
                            <div className="class-block">
                                <h4>My Favorites</h4>
                                <div className="row">
                                    {this.state.myFavorite.map((item, index) => {
                                        return (
                                            <VideoDetails item={item.video} key={item.video.id} />
                                        );
                                    })}
                                </div>

                                {this.state.myFavorite.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/my-favorites">View All</Link>
                                    </div>
                                    : null }
                                     {this.state.myFavorite.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">Tap the Favorites button and you’ll see your Favorites videos here.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                     : null 
                                }                                
                            </div>
                            <div className="class-block">
                                <h4>My Recently Watched</h4>
                                <div className="row">
                                    {this.state.recentlyWatched.map((item, index) => {
                                        return (
                                            <VideoDetails item={item.video} key={item.video.id} />
                                        );
                                    })}
                                </div>

                                {this.state.recentlyWatched.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/recent">View All</Link>
                                    </div>
                                    : null}
                                    {this.state.recentlyWatched.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }
                            </div>

                            <div className="class-block">
                                <h4>My Recommendations</h4>
                                <div className="row">
                                    {this.state.recommended.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id} />
                                        );
                                    })}
                                </div>
                                {this.state.recommended.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/recommended">View All</Link>
                                    </div>
                                    : null}
                                    {this.state.recommended.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                     : null
                                }
                            </div>
                            <div className="class-block mb-0">
                                <h4>Newly Added Videos</h4>
                                <div className="row">
                                    {this.state.newlyAdded.map((item, index) => {
                                        return (
                                            <VideoDetails item={item} key={item.id} />
                                        );
                                    })}
                                </div>
                                {this.state.newlyAdded.length > 3 ?
                                    <div className="text-right">
                                        <Link className="btn btn-sm" to="/user-dashboard/new-videos">View All</Link>
                                    </div>
                                     : null}
                                      {this.state.newlyAdded.length == 0 ?
                                    <div className="card-panel valign-wrapper grey lighten-4">
                                        <div className="row">
                                            <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                            <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center">
                                                <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                            </div>
                                        </div>
                                    </div>
                                    : null
                                }
                            </div>
                        </div>
                    </section>
                    <section className="sec sec-mobile pb-0">           
                        <div className="class-block">
                            <div className="container class-block-header">
                                <h4>My Classes</h4>
                                {this.state.myClasses.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/my-classes">View All</Link>
                                : null}
                            </div>
                             {this.state.myClasses.length == 0 ?
                                <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">Tap the My Classes button and you’ll see your Classes videos here.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 text-center mt-3">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                : 
                                <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.myClasses.map((item, index) => {
                                    return (
                                        <VideoDetailsMobile item={item.video} key={item.video.id} />
                                    );
                                })}
                                </OwlCarousel>
                            }
                        </div>
                        <div className="class-block">
                            <div className="container class-block-header">
                                <h4>My Favorites</h4>
                                {this.state.myFavorite.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/my-favorites">View All</Link>
                                : null }
                            </div>
                             {this.state.myFavorite.length == 0 ?
                             <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">Tap the Favorites button and you’ll see your Favorites videos here.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                : 
                                <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.myFavorite.map((item, index) => {
                                        return (
                                            <VideoDetailsMobile item={item.video} key={item.video.id} />
                                        );
                                    })}
                                </OwlCarousel>
                            }                                
                        </div>
                        <div className="class-block">
                            <div className="container class-block-header">
                                <h4>My Recently Watched</h4>
                                {this.state.recentlyWatched.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/recent">View All</Link>
                               
                                : null }
                            </div>
                              {this.state.recentlyWatched.length == 0 ?
                              <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                : 
                                <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.recentlyWatched.map((item, index) => {
                                        return (
                                            <VideoDetailsMobile item={item.video} key={item.video.id} />
                                        );
                                    })}
                                 </OwlCarousel>      
                            }
                        </div>

                        <div className="class-block">
                            <div className="container class-block-header">
                                <h4>My Recommendations</h4>
                                {this.state.recommended.length > 1 ?
                                    <Link className="btn btn-sm" to="/user-dashboard/recommended">View All</Link>
                                : null }
                            </div>
                            {this.state.recommended.length == 0 ?   
                            <div className="container">
                                <div className="card-panel valign-wrapper grey lighten-4">
                                    <div className="row">
                                        <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                        <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3 text-center">
                                            <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                :
                                <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                {this.state.recommended.map((item, index) => {
                                    return (
                                        <VideoDetailsMobile item={item} key={item.id} />
                                    );
                                })}
                            </OwlCarousel>
                            }
                        </div>
                        <div className="class-block mb-0">
                            <div className="container class-block-header">
                                <h4>Newly Added Videos</h4>
                                {this.state.newlyAdded.length > 1 ?
                                        <Link className="btn btn-sm" to="/user-dashboard/new-videos">View All</Link>
                                  
                                    : null }
                                </div>
                               
                                     {this.state.newlyAdded.length == 0 ?
                                     <div className="container">
                                        <div className="card-panel valign-wrapper grey lighten-4">
                                            <div className="row">
                                                <div className="col-xl-8 col-lg-8 col-md-8 col-sm-12 text-center">No data found in this category.</div>
                                                <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mt-3 text-center">
                                                    <Link className="btn btn-sm" to="/user-dashboard/search">Discover Now</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                     : 
                                     <OwlCarousel className="owl-theme" margin={10} nav={false} items={2} stagePadding={15} dots={false}>
                                     {this.state.newlyAdded.map((item, index) => {
                                         return (
                                             <VideoDetailsMobile item={item} key={item.id} />
                                         );
                                     })}
                                    </OwlCarousel> 
                                }
                            </div>
                    </section>
                </main>
                <Footer/>
            </div>
            </>
        )
    }
}
const mapStateToProps = (state) => ({
    alert: state.alert,
});

export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(Me);
