import React, { Component } from 'react';
import {Link} from "react-router-dom";
import Header from '../../components/user-dashboard/common/Header';
import Footer from '../../components/user-dashboard/common/Footer';
import axios from 'axios';
import SimpleReactValidator from 'simple-react-validator';
import { apiRoute, getApiHeader } from '../../utils/helpers';
import { connect } from 'react-redux';
import {Helmet} from "react-helmet";
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import { Multiselect } from 'multiselect-react-dropdown';
import {fetchSearchVideos} from '../../actions/videoAction';
import { InfiniteScroll } from 'react-simple-infinite-scroll';
import ReactTooltip from "react-tooltip";

class Search extends Component {

  constructor(props) {
    super(props);
    this.multiselectRef = React.createRef();
    this.state = {
      videos: [],
      teachers: [],
      styleType: [],
      intentions: [],
      styles: [],
      searchInput: '',
      duration: '',
      durationStart:'',
      selectedTeacher: '',
      selectedStyle: '',
      selectedStyleType: '',
      selectedTeacherText: '',
      selectedStyleText: '',
      selectedStyleTypeText: '',
      selectedIntention: [],
      selectedIntentionType: [],
      searchInputOptions:[],
      showFilterForm: false,
      showStyle: false,
    }
    this.searchValidation = new SimpleReactValidator();
    this.onChange = this.onChange.bind(this);
    this.toggleFilterForm = this.toggleFilterForm.bind(this);
    this.handleSearchForm = this.handleSearchForm.bind(this);
  }
  componentWillMount() {
    window.scrollTo(0, 0);
   
    const requestOptions = {
      headers: getApiHeader()
    };
    this.props.setLoadingTrue();
    if (this.props.location !== undefined && this.props.location.style !== undefined ) {
      this.setState({ selectedStyleType: this.props.location.style, showFilterForm: true, showStyle: true });
      const details = {
        searchInput: '',
        duration: '',
        durationStart:'',
        teacher: '',
        styleType: this.props.location.style,
        intentionType: [],
        style: '',
        limit:40,
        skip:0,
      };
      const stateValues = this.state;
      stateValues.selectedStyleType = this.props.location.style;
      stateValues.showFilterForm = true;
      stateValues.showStyle = true;
      axios.get(apiRoute('user-dashboard/get-styles/' + this.props.location.style), requestOptions)
        .then(res => {
          this.setState({ styles: res.data });
          stateValues.styles = res.data;
        });
       
       
        this.props.fetchSearchVideos(details,[],stateValues);
        axios.get(apiRoute('user-dashboard/get-all-video-teacher'), requestOptions)
          .then(res => {
            this.setState({ teachers: res.data });
          });
        axios.get(apiRoute('user-dashboard/get-all-videos-style'), requestOptions)
          .then(res => {
            this.setState({ styleType: res.data });
          });
        axios.get(apiRoute('user-dashboard/get-all-videos-intention'), requestOptions)
          .then(res => {
            this.setState({ intentions: res.data });
      });
    } else {
      this.setState(this.props.searchState);
      if(this.props.searchState.videos.length == 0){
        const details = {
          searchInput: this.props.searchState.searchInput,
          duration: this.props.searchState.duration,
          durationStart:this.props.searchState.durationStart,
          teacher: this.props.searchState.selectedTeacher,
          styleType: this.props.searchState.selectedStyleType,
          intentionType: this.props.searchState.selectedIntention,
          style: this.props.searchState.selectedStyle,
          limit:40,
          skip:0,
        };
        this.props.fetchSearchVideos(details,[],this.props.searchState);
        axios.get(apiRoute('user-dashboard/get-all-video-teacher'), requestOptions)
          .then(res => {
            this.setState({ teachers: res.data });
          });
        axios.get(apiRoute('user-dashboard/get-all-videos-style'), requestOptions)
          .then(res => {
            this.setState({ styleType: res.data });
          });
        axios.get(apiRoute('user-dashboard/get-all-videos-intention'), requestOptions)
          .then(res => {
            this.setState({ intentions: res.data });
          });
      }
     
    }
  }
  filterData = () => {
    const requestOptions = {
      headers: getApiHeader()
    };

    const details = {
      searchInput: this.state.searchInput,
      duration: this.state.duration,
      durationStart: this.state.durationStart,
      teacher: this.state.selectedTeacher,
      styleType: this.state.selectedStyleType,
      intentionType: this.state.selectedIntention,
      style: this.state.selectedStyle,
      limit:40,
      skip:0,
    };
    this.props.fetchSearchVideos(details,[],this.state);
  }

  onSelect = (selectedList, selectedItem) => {
    const selectedIds = [];
    selectedList.forEach(e => {
      selectedIds.push(e.id);
    });
    this.setState({ selectedIntention: selectedIds ,selectedIntentionType:selectedList});
    setTimeout(() => {
      this.filterData();
    }, 5);
  }

  onRemove = (selectedList, removedItem) => {

    const selectedIds = [];
    selectedList.forEach(e => {
      selectedIds.push(e.id);
    });
    var index = selectedIds.indexOf(removedItem);
    if (index > -1) {
      selectedIds.splice(index, 1);
    }
    this.setState({ selectedIntention: selectedIds });
    setTimeout(() => {
      this.filterData();
    }, 5);
  }

  onChange(e) {

    const index = e.target.selectedIndex;
    const optionElement = e.target.childNodes[index];
    const selectedName = optionElement.getAttribute('itemName');

    this.setState({ [e.target.name]: e.target.value });

    setTimeout(() => {
      this.filterData();
    }, 5);

    if (e.target.name === 'selectedStyleType') {
      if (e.target.value !== '') {
        this.setState({ showStyle: true, selectedStyle: '', selectedStyleTypeText:selectedName });
        const requestOptions = {
          headers: getApiHeader()
        };
        axios.get(apiRoute('user-dashboard/get-styles/' + e.target.value), requestOptions)
          .then(res => {
            this.setState({ styles: res.data });
          });
      } else {
        this.setState({ showStyle: false, styles: [], selectedStyle: '', selectedStyleTypeText:'' });
      }
    }
    if (e.target.name === 'selectedStyle') {
      if (e.target.value !== '') {
        this.setState({selectedStyleText:selectedName });
      }else{
        this.setState({selectedStyleText:'' });
      }
      
    }
    if (e.target.name === 'selectedTeacher') {
      if (e.target.value !== '') {
        this.setState({selectedTeacherText:selectedName });
      }else{
        this.setState({selectedTeacherText:'' });
      }
    }
  }

  handleSearchForm(e) {
    e.preventDefault();
    if (!this.searchValidation.allValid()) {
      this.searchValidation.showMessages();
      this.forceUpdate();
      return false;
    }
    this.setState({ searchInputOptions: [] });
    this.filterData();

  }

  toggleFilterForm() {
    this.setState({ showFilterForm: !this.state.showFilterForm });
  }

  changeFinterInput = (e) => {
    this.setState({ searchInput: e.target.value });
    if(e.target.value == ''){
      setTimeout(() => {
        this.filterData();
      }, 10);
    }
  }

  handleSearchOptiopnClick = (e) => {
    this.setState({ searchInput: e.target.innerHTML, searchInputOptions:[] });
  }

  clearAllFilter = (e) => {
    this.setState({
    searchInput: '',
    duration: '',
    durationStart:'',
    selectedTeacher: '',
    selectedStyleType: '',
    selectedTeacherText: '',
    selectedStyleText: '',
    selectedStyleTypeText: '',
    selectedIntention: [],
    selectedStyle: '',
    selectedIntentionType: [],
   });
   if(this.state.selectedIntention.length > 0){
    this.multiselectRef.current.resetSelectedValues();
   }
    setTimeout(() => {
      this.filterData();
    }, 5);
  }

  loadMore = () => {
    const details = {
      searchInput: this.state.searchInput,
      duration: this.state.duration,
      durationStart:this.state.durationStart,
      teacher: this.state.selectedTeacher,
      styleType: this.state.selectedStyleType,
      intentionType: this.state.selectedIntention,
      style: this.state.selectedStyle,
      limit:40,
      skip:this.props.cursor,
    };
    this.props.fetchSearchVideos(details,this.props.videos,this.state);
   }

   onDurationChange = (e) => {
    const index = e.target.selectedIndex;
    const optionElement = e.target.childNodes[index]
    const durationStart = optionElement.getAttribute('fromVal');
    this.setState({ duration: e.target.value });
    this.setState({ durationStart: durationStart });
    setTimeout(() => {
      this.filterData();
    }, 8);
   }

   removeStyle = (e) => {
    this.setState({ selectedStyle: '', selectedStyleText:'' });
    setTimeout(() => {
      this.filterData();
    }, 8);
   }

   removeStyleType = (e) => {
    this.setState({ selectedStyleType: '', selectedStyleTypeText:'' });
    setTimeout(() => {
      this.filterData();
    }, 8);
   }

   removeTeacther = (e) => {
    this.setState({ selectedTeacher: '', selectedTeacherText:'' });
    setTimeout(() => {
      this.filterData();
    }, 8);
   }

  render() {
    return (
      <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Search.</title>
    </Helmet>
      <div className="t3-wrapper">
        <Header />
        <main className="admin-content">
          <section className="inner-banner" style={{ background: 'url(/../images/bg-connect.jpg)', backgroundSize: 'cover', minHeight: '500px' }}>
            <div className="container text-center text-white">
              <h1>Search all our yoga classes, styles, intentions, teachers, and more.</h1>
            </div>
          </section>
          <section className="sec pt-4">
            <div className="container">
              <div className="sec-seaches">
                <div className="searchbar d-flex">
                    <form onSubmit={this.handleSearchForm} className="searchbar-bar d-flex">
                      <div className="searchbar-form">
                      <input type="text" placeholder="Search here.." autoComplete="off" name='searchInput' value={this.state.searchInput} onChange={this.changeFinterInput} />
                      <button className="btn btn-sm" type='submit'>Search here</button>
                      </div>
                      </form>
                      <div className="btn-searchs">
                        <button className="btn-floating btn-sm btn-filter" onClick={this.toggleFilterForm}><i className="fas fa-filter" /></button>
                        <button className="btn-floating btn-sm btn-filter ml-3" type='button' onClick={this.clearAllFilter} data-html={true} data-for='custom-color-no-arrow' data-tip='Clear all filters'><i class="fas fa-times"></i></button>
                        <ReactTooltip id='custom-color-no-arrow' className='react-tooltip' delayHide={1000} textColor='#FFF' backgroundColor='#000' effect='solid'/>
                      </div>
                  {/* <button className="btn btn-sm explore_surprise ml-2" data-toggle="modal" data-target="#surprise">Surprise Me</button> */}
                </div>
                {this.state.showFilterForm &&
                  <div className="search-filter-wrap">
                    <div className="row">
                      <div className="input-field col-md-3">
                        <select name='selectedStyleType' onChange={this.onChange}>
                          <option value="">Style</option>
                          {this.state.styleType.map((item, index) => {
                            return (
                              <option itemName={item.type} value={item.id} selected={this.state.selectedStyleType == item.id ? 'selected' : ''}>{item.type}</option>
                            );
                          })}
                        </select>
                      </div>   
                      {this.state.showStyle &&
                        <div className="input-field col-md-3">
                          <select name='selectedStyle' onChange={this.onChange}>
                            <option value="">Select Style</option>
                            {this.state.styles.map((item, index) => {
                              return (
                                <option itemName={item.name} value={item.id} selected={this.state.selectedStyle == item.id ? 'selected' : ''}>{item.name}</option>
                              );
                            })}
                          </select>
                        </div>
                      }                   
                      <div className="input-field col-md-3">
                        <select name='selectedTeacher' onChange={this.onChange}>
                          <option value='' selected>Select Teacher</option>
                          {this.state.teachers.map((item, index) => {
                            return (
                              <option itemName={item.name} value={item.id} selected={this.state.selectedTeacher == item.id ? 'selected' : ''}>{item.name}</option>
                            );
                          })}
                        </select>
                      </div>
                      <div className="input-field col-md-3">
                        <select name='duration' value={this.state.duration} onChange={this.onDurationChange}>
                          <option value='' selected>Duration</option>
                          <option fromVal={5400} value={7200}>2 Hours</option>
                          <option fromVal={4500} value={5400}>90 Minutes</option>
                          <option fromVal={3600} value={4500}>75 Minutes</option>
                          <option fromVal={2700} value={3600}>60 Minutes</option>
                          <option fromVal={1800} value={2700}>45 Minutes</option>
                          <option fromVal={1200} value={1800}>30 Minutes</option>
                          <option fromVal={900} value={1200}>20 Minutes</option>
                          <option fromVal={600} value={900}>15 Minutes</option>
                          <option fromVal={300} value={600}>10 Minutes</option>
                          <option fromVal={0} value={300}>5  Minutes</option>
                        </select>
                      </div>
                      
                      <div className="col-md-12">
                        <Multiselect
                          options={this.state.intentions}
                          onSelect={this.onSelect}
                          onRemove={this.onRemove}
                          ref={this.multiselectRef}
                          selectedValues={this.state.selectedIntentionType}
                          displayValue="name"
                          placeholder="Select Intentions"
                        />

                      </div>
                      <div className="col-md-12">
                      {/* <div className="text-right">
                      <button className="btn btn-sm" type='button' onClick={this.clearAllFilter}>Reset filter</button>
                   </div> */}
                   </div>
                    </div>
                    <div className="chipList">
                    {this.state.selectedStyleTypeText !== '' ?
                        <span class="chip">{this.state.selectedStyleTypeText} <i class="fa fa-times-circle-o" aria-hidden="true" onClick={this.removeStyleType}></i></span>
                        : 
                        '' 
                        }

                      {this.state.selectedStyleText !== '' ?
                        <span class="chip">{this.state.selectedStyleText} <i class="fa fa-times-circle-o" aria-hidden="true" onClick={this.removeStyle}></i></span>
                        : 
                        '' 
                        }
                   
                    {this.state.selectedTeacherText !== '' ?
                        <span class="chip">{this.state.selectedTeacherText} <i class="fa fa-times-circle-o" aria-hidden="true" onClick={this.removeTeacther}></i></span>
                        : 
                        '' 
                        }
                </div>
              </div>
                }
               
              </div>
              <div className="class-block my-0 border-0">
                <div className>
                  <h4 className="vid_stat_cnt"><span id="total_rec">{this.props.totalCount}</span> Videos</h4>
                </div>
               
                <InfiniteScroll
                    throttle={100}
                    threshold={300}
                    isLoading={alert.isloading}
                    hasMore={this.props.hasMore}
                    onLoadMore={this.loadMore}
                >
                        <div className="row">
                       
                        {this.props.videos.map((item,index)=>{
                        return(
                          <div className="col-xl-3 col-lg-3 col-md-4 col-sm-6 col-6" key={item.id}>
                            <Link to={ {pathname: "/user-dashboard/video-details/" + item.id, state:this.state} }>
                              <div className="hoverable card">
                                <img className="img-fluid" src={item.thumbnail} />
                                <div className="card-content">
                                  <span className="card-title" data-html={true} data-for='custom-color-no-arrow' data-tip={item.title}>{item.title}</span>
                                  <ReactTooltip id='custom-color-no-arrow' className='react-tooltip card-title-tooltip' delayHide={1000} textColor='#FFF' backgroundColor='#5c1b72' effect='solid'/>
                                  <p className="pop" dangerouslySetInnerHTML={{ __html: item.description }}></p>
                                </div>
                              </div>
                            </Link>
                          </div>
                            );
                            })}
                            </div>
                            </InfiniteScroll>
                  {this.props.videos.length > 0 ?
                    null
                    :
                    <div className="card-panel text-center sattva-error">
                      <p>No videos found, please try again later, Thank you</p>
                    </div>
                  }
              </div>
            </div>
          </section>
        </main>
        <Footer/>
      </div>
      </>
    )
  }
}
const mapStateToProps = (state) => ({
  videos: state.video.items,
  alert: state.alert,
  cursor:state.video.cursor,
  hasMore:state.video.hasMore,
  totalCount:state.video.totalCount,
  searchState:state.video.searchState,
});
export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts, fetchSearchVideos })(Search);