import React, { Component } from 'react';
import {Helmet} from "react-helmet";
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';

class PageNotFound extends Component {

	
	render() {

		return (
			<>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>Page Not Found.</title>
                <meta name="description" content="Terms of services." />
            </Helmet>
				<Header />
				<main class="admin-content">
					<section class="sec">
						<div class="container">
							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="thankyou-div">
										<img src="../../images/error-red.png" class="img-fluid" />
										<h1>404!</h1>
										<h3>PAGE NOT FOUND</h3>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<Footer />
			</>
		);
	}
}

export default PageNotFound;