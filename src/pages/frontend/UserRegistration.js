import React from 'react';
import {Helmet} from "react-helmet";
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';
import RegistrationForm from '../../components/frontend/auth/RegistrationForm';

export default function UserRegistration(props) {
    const planId = props.match.params.planId;
    return (
        <>
         <Helmet>
            <meta charSet="utf-8" />
            <title>Sign up | Unlimited access to hundreds of yoga classes online</title>
            <meta name="description" content="A monthly and yearly subscription gives you unlimited access to hundreds of yoga classes online. Be inspired. Live inspired. Sattva Connect." />
            <link rel="canonical" href="https://www.sattvaconnect.com/user-registration/2"/>
        </Helmet>
        <div className="t3-wrapper">
            <Header />
            <main>
                <div className="sec sec-cinfo">
                    <div className="container">
                        
                        {alert.message && alert.type === 'error' &&
                            <div className="alert alert-danger" role="alert">
                                {alert.message}
                            </div>}
                        {alert.message && alert.type === 'success' &&
                            <div className="alert alert-success" role="alert">
                                {alert.message}
                            </div>}
                        <div className="customer-support p-0">
                        <h4>{planId == 5 ? 'Yearly' : 'Monthly' } Subscription</h4>
                        <div className="card-panel sattva-error">
                            <p>Please enter information on the form below to process subscription for <strong>{planId == 5 ? 'Yearly' : 'Monthly' }</strong>. The price&nbsp;<strong>${planId == 5 ? '210' : '21' }</strong>.</p>
                            </div>
                            <RegistrationForm planId={planId}/>
                        </div>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
        </>
    )
}
