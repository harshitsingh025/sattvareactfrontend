import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {Helmet} from "react-helmet";
import { apiRoute, getApiHeader,getLocalStorageAuth } from '../../utils/helpers';
import {setLoadingTrue, clearAllAlerts} from '../../actions/userAction';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';

class UpcomingStream extends Component {

	constructor(props) {
		super(props);
		this.state = {
			banner : {
				image:'anadlivestreambanner_1590682874.jpg',
				title:'Upcoming Live Streams',
				description:'',
				link:'',
			  },
			sections: [[], [], [],],
			streamCalendar:[],
		}
	}

	componentDidMount() {
		window.scrollTo(0, 0);
		
		const requestOptions = {
			headers: getApiHeader()
		};
		this.props.setLoadingTrue();

		// axios.get(apiRoute('cms-page-banner/' + btoa(18)), requestOptions)
		// 	.then(res => {
		// 		this.setState({ banner: res.data });
		// 		this.props.clearAllAlerts();
		// 	});

		axios.get(apiRoute('get-upcomming-streams'), requestOptions)
		.then(res => {
			this.setState({ streamCalendar: res.data });
			this.props.clearAllAlerts();
		});	

	}

	render() {
		let bannerImage = require('../../assets/images/banner/'+this.state.banner.image);
		var banner = {
			backgroundImage: `url(${bannerImage})`
		}
		return (
			<>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>Join us LIVE | Sattva Connect Broadcast Channel</title>
				<meta name="description" content="Sattva Connect brings you with LIVE streaming of authentic teachings and yogic practices. Our classes are recorded and uploaded to our platform." />
				<meta name="keywords" content="Live Streaming Yoga, Live Stream Yoga Classes" />
			</Helmet>
				<Header />
				<div className="view intro-2">
			    <section className="inner-banner" style={banner}>
			      <div className="container text-center text-white">
					{this.state.banner.title ? 
						<h1>{this.state.banner.title}</h1> : ''
					}
						{this.state.banner.description ? 
						<h2 className="sp-posttitle animated-two fadeInUp sp-animation-2">{this.state.banner.description}</h2> : ''
					}
						{this.state.banner.link ? 
							<div className="read-more-wrapper animated-four fadeInUp">
							<a href={this.state.banner.link} className="btn waves-effect waves-light btn-lg">Learn more</a>
							</div> : ''
						}
						</div>
						</section>
					</div>
				<main>
					<section className="sec sec-inabout">
						<div className="container">
							<div className="row">
								<div className="col-md-12">
								<p>Welcome to Sattva Connect's Live Stream Channel. Below is an overview of upcoming live stream events. Please note that you must be a registered member of Sattva Connect to be able to join an ongoing live stream event.</p>
								</div>
							</div>
							{this.state.streamCalendar.map((item,index)=>{
							return(
		        				<div className="upcminglivestream">
								<div className="card">
									<div className="row">
										<div className="col-xl-4 clo-lg-4 col-md-4 col-sm-6">
											<div className="card-image"><img src={item.image} className="img-fluid" alt="" /></div>
										</div>
										<div className="col-xl-8 clo-lg-8 col-md-8 col-sm-6 mt-sm-0 mt-3">
											<h4 className="h4-style">{item.name}</h4>
												<p><strong>{item.description} </strong></p>
												<p className="underline"><span><strong>Schedule:</strong></span> </p>
												{item.schedule.map((items,index)=>{
											return(
												<p>{items}</p>
												);
											})}	
										</div>
									</div>
								</div>
							</div>	);
						})}	
					</div>
					</section>
					<section className="sec upcoming-last-sec">
						<div className="container">
							<div className="row">
								<div className="col-md-12 text-white text-center">
									<h2>You have to be a member in order to join a Live Stream event.</h2>
									<Link to="/plans" className="btn btn-lg mt-3">Sign Up</Link>
								</div>
							</div>
						</div>
					</section>
				</main>
				<Footer/> 
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	alert: state.alert,
});
export default connect(mapStateToProps, {setLoadingTrue, clearAllAlerts})(UpcomingStream);