import React,{Component} from 'react';
import {Helmet} from "react-helmet";
import { Link } from 'react-router-dom';
import Footer from '../../components/frontend/common/Footer';
import Header from '../../components/frontend/common/Header';

class SupportSuccess extends Component{
	
	  componentDidMount() {
		window.scrollTo(0, 0);
	 }
	
	render(){
		
		return(
			<>
			<Helmet>
                <meta charSet="utf-8" />
                <title>Support success.</title>
                <meta name="description" content="Registration success." />
            </Helmet>
			<div className="t3-wrapper">
			<Header/>
				<main>
				<section className="sec">
				<div className="container">
					<div className="row">
					<div className="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class="thankyou-div">
							<img src={require("../../assets/images/correct-green.png")} className="img-fluid"/>
							<h3>Thank You!</h3>
							<h6>Your request is sent successfully.</h6>
							<p>You can expect to hear back from us within 48 hours.</p>
							<a className="waves-effect btn" href="customer-support">SEND A NEW INQUIRY</a>
						</div>
					</div>
					</div>
				</div>
				</section>  
			</main>
		    <Footer/>
			</div>
			</>
			);
	}
}

export default SupportSuccess;