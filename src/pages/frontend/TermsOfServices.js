import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {Helmet} from "react-helmet";
import { apiRoute, getApiHeader, getLocalStorageAuth } from '../../utils/helpers';
import {setLoadingTrue, clearAllAlerts} from '../../actions/userAction';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';
import MobileFooter from '../../components/user-dashboard/common/Footer';

class TermsOfServices extends Component{

	constructor(props){
		super(props);
		const userData = getLocalStorageAuth();
		if(!userData){
			this.state = {
			   auth : false,
			   }
		   }else{	
		   this.state = {
			   auth : true,
			   }	
		   }
	      this.state = {
	      	pageData : [
	      	[]
	      	],
		    } 
		  }

	componentWillMount() {
		
    window.scrollTo(0, 0);

    this.props.setLoadingTrue();

   	 const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-page-data/'+btoa(9)),requestOptions)
		.then(res =>{
			this.setState({ pageData : res.data });
			this.props.clearAllAlerts();
	    })
	}

	render(){
		return(
			<>
			<Helmet>
                <meta charSet="utf-8" />
                <title>Terms of services.</title>
                <meta name="description" content="Terms of services." />
            </Helmet>
			<div className="t3-wrapper">
			<Header/>
			<main> 
			  <section className="sec sec-inabout sec-privacy">
			      <div className="container" dangerouslySetInnerHTML={{__html: this.state.pageData['0'].description}}>
			     </div>
			    </section>  
			 </main>
			 {!this.state.auth &&
				<Footer/>
				}
				<MobileFooter/>
			</div>
			</>
			);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {setLoadingTrue, clearAllAlerts})(TermsOfServices);