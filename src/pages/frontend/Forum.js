import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../utils/helpers';
import Banner from '../../components/frontend/common/Banner';
import {setLoadingTrue, clearAllAlerts} from '../../actions/userAction';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';

class Forum extends Component{

	constructor(props){
    	super(props);
	      this.state = {
	      	banner : [],
	        forums: [],
		    } 
		  }

	componentWillMount() {
		
    window.scrollTo(0, 0);

    this.props.setLoadingTrue();

    const requestOptions = {
			headers : getApiHeader()
		};

		axios.get(apiRoute('cms-page-banner/'+btoa(16)),requestOptions)
		.then(res =>{
			this.setState({ banner : res.data });
	    })

	    axios.get(apiRoute('get-all-forums'),requestOptions)
		.then(res =>{
			this.setState({ forums : res.data });
			this.props.clearAllAlerts();
	    })

	}
	render(){
		return(
			<>
			<Header/>
			 <Banner banner={this.state.banner}/>
			  <main>
			  {this.state.forums && this.state.forums.length !== 0 ? 
			   <section className="sec sec-inabout">
			        <div className="container">
			            <div className="row">
			             {this.state.forums.map((item,index)=>{
			             	return(
			                <div className="col-md-6">
			                    <div className="card">
			                        <div className="card-content insp-content">
			                            <h5>{item.title}</h5>
			                            <p>{item.description}</p>
			                            <a className="btn btn-sm waves-effect waves-light">Read More</a>
			                        </div>
			                    </div>
			                </div>
			                );
			       		})}	        
			            </div>
			        </div>
			    </section>
			      : '<h3>No Data Found</h3>'}
			  </main>
			<Footer/>
			</>
			);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {setLoadingTrue, clearAllAlerts})(Forum);