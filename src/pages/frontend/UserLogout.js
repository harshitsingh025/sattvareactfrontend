import React,{Component} from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { setLocalStorageAuth } from '../../utils/helpers';

class UserLogout extends Component{

	constructor(props){
    super(props);
    setLocalStorageAuth(false);
    }
	  
	render(){
		return(
		<Redirect to={window.PUBLIC_URL+'/'}/>
		);
}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});

export default connect(mapStateToProps, null)(UserLogout);