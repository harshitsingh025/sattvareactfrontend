import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Helmet} from "react-helmet";
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';
import LoginForm from '../../components/frontend/auth/LoginForm';

class Login  extends Component {
    constructor(props) {
		super(props);
	}
    render() {
		const { alert } = this.props;
    return (
        <>
          <Helmet>
                <meta charSet="utf-8" />
                <title>Login | Unlimited access to hundreds of yoga classes online</title>
				<meta name="description" content="Practice with Sattva Connect today! Find ease and peace, greater joy and love. Sattva Connect is Your Support to an Awakened Life." />
				<meta name="keywords" content="yoga videos online, yoga for beginners online, online yoga subscription, yoga for beginners, Live yoga classes online" />
                <link rel="canonical" href="https://www.sattvaconnect.com/login/"/>
            </Helmet>
        <div className="t3-wrapper">
            <Header />
            <main>
                <div className="sec sec-cinfo">
                    <div className="container">
                        <LoginForm history={this.props.history}/>  
                                             
                    </div>
                </div>
            </main>
            <Footer />
        </div>
        </>
    )
    }
}
const mapStateToProps = (state) => ({
	alert: state.alert,
});
export default connect(mapStateToProps)(Login);
