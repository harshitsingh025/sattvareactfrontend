import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../utils/helpers';
import {ToastsContainer, ToastsStore, ToastsContainerPosition} from 'react-toasts';
import SimpleReactValidator from 'simple-react-validator';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';
import Banner from '../../components/frontend/common/Banner';
import TeacherImages from '../../components/frontend/teacher/TeacherImages';
import TeacherVideos from '../../components/frontend/teacher/TeacherVideos';
import TeacherCourses from '../../components/frontend/teacher/TeacherCourses';
import TeacherEvents from '../../components/frontend/teacher/TeacherEvents';
import { imagePath } from '../../utils/helpers';
import * as TeacherActions  from '../../actions/teacherAction';

class TeacherDetails extends Component{

	constructor(props){
		super(props);
	    this.validator = new SimpleReactValidator();
	    this.state = {
	  		email : '',
	  		subject : '',
	  		message: '',
	  		toEmail: '',
	  		modalStatus: 'none',
	  		banner : {},
	  }	
	  this.onChange = this.onChange.bind(this);
	  this.onSubmit = this.onSubmit.bind(this);
	  this.onModalClose = this.onModalClose.bind(this);
	  this.resetForm = this.resetForm.bind(this);
	  this.setTeacherEmail = this.setTeacherEmail.bind(this);
	}

	componentDidMount() {
	    window.scrollTo(0, 0);
	    this.props.fetchTeacherDetails(this.props.match.params.teacherId, this.props.history);
	}

	componentWillMount() {

   	 const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-page-banner/'+btoa(12)),requestOptions)
		.then(res =>{
			this.setState({ banner : res.data });
	    })
	}

	onChange(e){
		this.setState({ [e.target.name] : e.target.value });
	}

	onSubmit(e){
		e.preventDefault();
		if (!this.validator.allValid()) {
            this.validator.showMessages();
            this.forceUpdate();
            return false;
        }	
    	const userDetails = {
			email: this.state.email,
			subject: this.state.subject,
			message: this.state.message,
			toEmail: this.state.toEmail,
		}	
        this.props.sendTeacherEmail(userDetails)
        this.onModalClose();
	}

	setTeacherEmail(email){
		this.resetForm();
		this.setState({
			toEmail: email,
			modalStatus: 'block'
		});
	}

	onModalClose(){
		this.resetForm();
		this.setState({
			modalStatus: 'none'
		});
	}

	resetForm(){
		this.validator.hideMessages();
		document.getElementById("enquiryForm").reset();
		this.setState({
			email: '',
			subject: '',
			message: '',
			toEmail: '',
		});
	}

	render(){

		const teacherDetais = this.props.teachersDetails;
		var modalStatus = {
			display: this.state.modalStatus,
		}

		const { alert } = this.props;

		if(alert && alert.message){
		 var alertMessage = alert.message;
		 var alertType = alert.type;
		 this.props.resetAlertValues();
		}

		return(
			<>
			{alertMessage && alertType === 'success' &&
                  ToastsStore.success(alertMessage) }
            {alertMessage && alertType === 'error' &&
                  ToastsStore.error(alertMessage) } 
            <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.TOP_RIGHT}/>      
			<Header/>
			<Banner banner={this.state.banner}/>
			<main>
			 <section className="sec-profile">
		      <div className="container">
				<div className="col-md-12">
					<div className="row">
						<div className="card p-4">
							<div className="col-md-3">
								<div className="row">
									<div className="profile-left text-center profile-teacher">
										<div className="profile-img">
										<img src={imagePath('teachers/'+ teacherDetais.image)} />
										</div>
										<ul>
										{ teacherDetais.facebook
										? <li><a href={teacherDetais.facebook} target="_blank"><i className="fab fa-facebook-f"></i></a></li>
										: ''
									}
									{ teacherDetais.twitter
										? <li><a href={teacherDetais.twitter} target="_blank"><i className="fab fa-twitter"></i></a></li>
										: ''
									}
									{ teacherDetais.instagram
										? <li><a href={teacherDetais.instagram} target="_blank"><i className="fab fa-instagram"></i></a></li>
										: ''
									}
										</ul>
										<a className="btn btn-sm"  onClick={this.setTeacherEmail.bind(null,teacherDetais.email)}>Send Email</a>
									</div>
								</div>
							</div>										
							<div className="col-md-9">
								<div className="row">
									<div className="profile-content">
									<h5>{teacherDetais.name}</h5>
									<p>{teacherDetais.description}</p>
									<div className="profile-points">
									<ul className="profile-ul">
										<li className="specialities">Specialities : </li>
										<li>
											<ul>
												{teacherDetais.specialities && teacherDetais.specialities.map((items,index) => {
											return(
												<li key={index}><p>{items.name}</p></li>
												);
											})}
											</ul>
										</li>
									</ul>		              
									</div>
								</div>
								</div>
							</div>	
						</div>
						<div className="card d-block">
						<div className="card-panel my-0">
							<ul className="nav nav-tabs custom-tabs nowrap" id="myTab" role="tablist">
							<li className="nav-item">
								<a className="nav-link active" id="more-about-tab" data-toggle="tab" href="#more-about" role="tab" aria-controls="more-about"
								aria-selected="true">More About Anandji</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" id="images-tab" data-toggle="tab" href="#images" role="tab" aria-controls="images"
								aria-selected="false">Images</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" id="videos-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos"
								aria-selected="false">Videos</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" id="courses-tab" data-toggle="tab" href="#courses" role="tab" aria-controls="courses"
								aria-selected="false">Courses</a>
							</li>
							<li className="nav-item">
								<a className="nav-link" id="events-tab" data-toggle="tab" href="#events" role="tab" aria-controls="events"
								aria-selected="false">Events</a>
							</li>
							</ul>
							<div className="tab-content" id="myTabContent">
							<div className="tab-pane fade show active" id="more-about" role="tabpanel" aria-labelledby="more-about-tab">
								<table className="table table-striped profile-table teacher-table">
								<tbody>
									{teacherDetais.info && teacherDetais.info.map((items,index) => {
									return(
										<tr>
										<th key={index}>{items.title}</th>
										<td key={index}>{items.description}</td>
										</tr>
										);
									})}
									{teacherDetais.info && teacherDetais.info.length == 0 ?<tr><td className="p-0"><div className="no_found mt-3">No Data Found</div></td></tr> : ''}
								</tbody>
								</table>
							</div>
							<div className="tab-pane fade" id="images" role="tabpanel" aria-labelledby="images-tab">
								<section className="gallery-block grid-gallery pt-0">
									<div className="row"> 
									{teacherDetais.images && teacherDetais.images.map((items,index) => {
									return(
											<TeacherImages image={items}/>
										);
									})}
									{teacherDetais.images && teacherDetais.images.length == 0 ?<div className="col-md-12"><div className="no_found mt-3">No Data Found</div></div> : ''}
									</div>
								</section>
							</div>
							<div className="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab">
								{teacherDetais.videos && teacherDetais.videos.map((items,index) => {
								return(
										<TeacherVideos video={items}/>
									);
								})}
								{teacherDetais.videos && teacherDetais.videos.length == 0 ?<div className="no_found mt-3">No Data Found</div> : ''}
							</div>
							<div className="tab-pane fade" id="courses" role="tabpanel" aria-labelledby="courses-tab">
								{teacherDetais.course && teacherDetais.course.map((items,index) => {
								return(
										<TeacherCourses course={items}/>
									);
								})}
								{teacherDetais.course && teacherDetais.course.length == 0 ?<div className="no_found mt-3">No Data Found</div> : ''}
							</div>
							<div className="tab-pane fade" id="events" role="tabpanel" aria-labelledby="events-tab">
								{/*<div className="owl-carousel owl-theme events-carousel mt-4">
								<div className="item activeitem">
									<a className="nav-link" id="january-tab" data-toggle="tab" href="#january" role="tab" aria-controls="january-tabs"
									aria-selected="true">January</a>
								</div>
								<div className="item">
									<a className="nav-link" id="february-tab" data-toggle="tab" href="#february" role="tab" aria-controls="february-tabs"
									aria-selected="false">February</a>
								</div>
								<div className="item">
									<a className="nav-link" id="march-tab" data-toggle="tab" href="#march" role="tab" aria-controls="march-tabs"
									aria-selected="false">March</a>
								</div>
								<div className="item">
									<a className="nav-link" id="april-tab" data-toggle="tab" href="#april" role="tab" aria-controls="april-tabs"
									aria-selected="false">April</a>
								</div>
								<div className="item">
									<a className="nav-link" id="may-tab" data-toggle="tab" href="#may" role="tab" aria-controls="may-tabs"
									aria-selected="false">May</a>
								</div>
								<div className="item">
									<a className="nav-link" id="june-tab" data-toggle="tab" href="#june" role="tab" aria-controls="june-tabs"
									aria-selected="false">June</a>
								</div>
								<div className="item">
									<a className="nav-link" id="july-tab" data-toggle="tab" href="#july" role="tab" aria-controls="july-tabs"
									aria-selected="false">July</a>
								</div>
								<div className="item">
									<a className="nav-link" id="august-tab" data-toggle="tab" href="#august" role="tab" aria-controls="august-tabs"
									aria-selected="false">August</a>
								</div>
								<div className="item">
									<a className="nav-link" id="september-tab" data-toggle="tab" href="#september" role="tab" aria-controls="september-tabs"
									aria-selected="false">September</a>
								</div>
								<div className="item">
									<a className="nav-link" id="october-tab" data-toggle="tab" href="#october" role="tab" aria-controls="october-tabs"
									aria-selected="false">October</a>
								</div>
								<div className="item">
									<a className="nav-link" id="november-tab" data-toggle="tab" href="#november" role="tab" aria-controls="november-tabs"
									aria-selected="false">November</a>
								</div>
								<div className="item">
									<a className="nav-link" id="december-tab" data-toggle="tab" href="#december" role="tab" aria-controls="december-tabs"
									aria-selected="false">December</a>
								</div>
								</div>*/}
								<div className="tab-content">
								<div className="tab-pane active" id="january" role="tabpanel" aria-labelledby="january-tab">
									<div className="events-intabs">
									{teacherDetais.events && teacherDetais.events.map((items,index) => {
									return(
											<TeacherEvents event={items}/>
										);
									})}
									{teacherDetais.events && teacherDetais.events.length == 0 ?<div className="no_found mt-3">No Data Found</div> : ''}
									</div>
								</div>
								</div>
							</div>
							</div>
						</div>
						</div>
					
					</div>
				</div>
			  </div>
		    </section>
			</main>
			<Footer/>
			<div className="modal fade custom-modal show" id="send-mail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
			  aria-hidden="true" style={modalStatus}>
			  <div className="modal-dialog" role="document">
			    <div className="modal-content">
			      <button type="button" className="close" aria-label="Close" onClick={this.onModalClose}>
			        <span aria-hidden="true">&times;</span>
			      </button>
			      <div className="modal-body">
			        <form onSubmit={this.onSubmit} id='enquiryForm'>
			             <div className="input-field position-relative">
			                <label id="email-lbl" for="email" className=" required">Email Address<span className="star">&#160;*</span></label>
			                <input type="email" name="email" id="email" value={this.state.email} onChange={this.onChange}/>
			                {this.validator.message('email', this.state.email, 'required|email')}
			             </div>
			             <div className="input-field position-relative">
			                <label id="subject-lbl" for="subject" className=" required">Subject<span className="star">&#160;*</span></label>
			                <input type="text" name="subject" id="subject" value={this.state.subject} onChange={this.onChange}/>
			                {this.validator.message('subject', this.state.subject, 'required')}
			             </div>
			             <div className="input-field position-relative">
			                <label id="message-lbl" for="message" className=" required">Message<span className="star">&#160;*</span></label>
			                <textarea id="message" className="md-textarea form-control" rows="2" name="message" onChange={this.onChange}>{this.state.message}</textarea>
			                {this.validator.message('message', this.state.message, 'required')}
			             </div>
			            <div className="input-field s12 text-right">
			            	<input type="hidden"id="toEmail" value={this.state.toEmail} name="toEmail"/>
			              <button className="btn btn-lg" type="submit" >Send Now</button>
			            </div>
			        </form>
			      </div>
			    </div>
			  </div>
			</div>	
			</>
			);
	}
}

const mapStateToProps = (state) =>({
  teachersDetails: state.teacherDetails.item,
  alert: state.alert,
});

const mapActionsToProps = ({
    fetchTeacherDetails: TeacherActions.fetchTeacherDetails,
    sendTeacherEmail:TeacherActions.sendTeacherEmail,
    resetAlertValues:TeacherActions.resetAlertValues,
})

export default connect(mapStateToProps, mapActionsToProps)(TeacherDetails);