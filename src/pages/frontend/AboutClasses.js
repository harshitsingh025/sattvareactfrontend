import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';
import {Helmet} from "react-helmet";
import Vimeo from '@u-wave/react-vimeo';
import axios from 'axios';
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import { apiRoute , getApiHeader ,imagePath } from '../../utils/helpers';

class AboutClasses extends Component {


	constructor(props){
        super(props);
        this.hariomModal = React.createRef();
          this.state = {
			showVideo:true,
			videos:[],
			currentVideo:'',
			currentTitle:'',
        } 
      }
	  componentDidMount() {
		window.scrollTo(0, 0);
		
		const requestOptions = {
			headers : getApiHeader()
		};
		this.props.setLoadingTrue();
		axios.get(apiRoute('classes-video-list'),requestOptions)
		//axios.get('https://sattvastaging.website/backendportal/api/classes-video-list')
		.then(res =>{
			this.props.clearAllAlerts();
			this.setState({'videos':res.data.videos});
		})	
		.catch(error => {
			this.props.clearAllAlerts();
		});
	}

	
    currentTime = (e) => {
		const seconds = e.seconds;
		if(seconds > 300){
			this.setState({ showVideo : false });
		}
	}

	setAccessTrue = () => {
		this.setState({ showVideo : true });
	}

	setVideoUrl = (url, title) => {
		this.setState({  currentVideo: url, currentTitle:title});
	}

	render() {
		console.log(this.state.currentVideo);
		return (
			<>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>About Classes.</title>
				<link rel="canonical" href="https://www.sattvaconnect.com/about-classes/"/>
            </Helmet>
				<div className="t3-wrapper">
					<Header />
					<main>
						<section className="sec sec-inclasses">
							<div className="container">
								<div className="class-block mt-0">
									<h4 className="h4-style">Evolution</h4>
									<div className="row">
									{ this.state.videos.evolution && this.state.videos.evolution.map((item,index)=>{
										 
										 return(
											 <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
												 <div className="hoverable card" modalTitle={item.title}  modalVideo={item.video_url} id="modalEvo" >
													 <a className="modal-trigger" data-backdrop="static" data-toggle="modal" data-target="#vitality3" onClick={() => this.setVideoUrl(item.video_url,item.title)}>
														 <img className="img-fluid" src={imagePath('frontendVideo/'+item.image_url)} />
													 </a>
													 
												 </div>
											 </div>
												 
									  );
									 })}	
										
									</div>
								</div>
								<div className="class-block">
									<h4 className="h4-style">Clarity</h4>
									<div className="row">
									{ this.state.videos.clarity && this.state.videos.clarity.map((item,index)=>{
										 
										 return(
											 <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
												 <div className="hoverable card" modalTitle={item.title}  modalVideo={item.video_url} id="modalEvo" >
													 <a className="modal-trigger" data-backdrop="static" data-toggle="modal" data-target="#vitality3" onClick={() => this.setVideoUrl(item.video_url,item.title)}>
														 <img className="img-fluid" src={imagePath('frontendVideo/'+item.image_url)} />
													 </a>
													 
												 </div>
											 </div>
												 
									  );
									 })}	

									</div>
								</div>
								<div className="class-block">
									<h4 className="h4-style">Vitality</h4>
									<div className="row">
									{ this.state.videos.vitality && this.state.videos.vitality.map((item,index)=>{
										 
										 return(
											 <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
												 <div className="hoverable card" modalTitle={item.title}  modalVideo={item.video_url} id="modalEvo" >
													 <a className="modal-trigger" data-backdrop="static" data-toggle="modal" data-target="#vitality3" onClick={() => this.setVideoUrl(item.video_url,item.title)}>
														 <img className="img-fluid" src={imagePath('frontendVideo/'+item.image_url)} />
													 </a>
													 
												 </div>
											 </div>
												 
									  );
									 })}	
									</div>
								</div>
								<div className="modal fade iframe-modal" id="vitality3" tabindex="-1" role="dialog" aria-labelledby="vitality3Title" aria-hidden="true">
									<div className="modal-dialog modal-lg modal-dialog-centered" role="document">
										<div className="modal-content">
											<div className="modal-header">
												<h6 className="mb-3">{this.state.currentTitle}</h6>
												<button type="button" className="close closeModal" data-dismiss="modal">&times;</button>
											</div>
											<div className="modal-body">
												<div className="video-container">
													{/* <iframe className="ifmplayer" src="https://player.vimeo.com/video/279921630?color=ffffff&amp;title=0&amp;byline=0&amp;portrait=0" data-ready="true" frameborder="0"></iframe> */}
													{this.state.currentVideo ?
														<Vimeo
														video={this.state.currentVideo}
														onTimeUpdate={this.currentTime}
														/>:
															<div className="accessErrorMsg">
																<h3>Login/Sign up to continue further.</h3>
																<div className="goToButtonsDiv">
																<a href="/login" className="btn btn-sm ">LOGIN</a>
																<a href="/plans" className="btn btn-sm ">SIGN UP</a>
																</div>
															</div>
														}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</main>
					<Footer />
				</div>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	alert: state.alert,
});
export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(AboutClasses);