import React,{Component} from 'react';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../utils/helpers';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';
import Banner from '../../components/frontend/common/Banner';
import TeacherList from '../../components/frontend/teacher/TeacherList';

class Teachers extends Component{

	constructor(props){
    super(props);
      this.state = {
      	banner : {},
	    } 
	  }


	componentDidMount() {

   	 window.scrollTo(0, 0);

   	 const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-page-banner/'+btoa(6)),requestOptions)
		.then(res =>{
			this.setState({ banner : res.data });
	    })
	}
	
	render(){
		return(
			<>
			<Header/>
			<Banner banner={this.state.banner} />
			<main>
			<TeacherList/>
			</main>
			<Footer/>
			</>
			);
	}
}

export default Teachers;