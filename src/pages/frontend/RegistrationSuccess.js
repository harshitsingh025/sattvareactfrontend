import React,{Component} from 'react';
import {Helmet} from "react-helmet";
import ReactPixel from 'react-facebook-pixel';
import Footer from '../../components/frontend/common/Footer';
import Header from '../../components/frontend/common/Header';
import { getLocalStorageAuth } from '../../utils/helpers';

class RegistrationSuccess extends Component{
	state = {
		redirect: false,
		count: 3,
	  }
	  componentDidMount() {
		window.scrollTo(0, 0);
		const userData = getLocalStorageAuth();

		if (userData) {
			ReactPixel.init('2255546191382402');
			ReactPixel.pageView();
			ReactPixel.track('CompleteRegistration',  {} ) 
		}
		
		setTimeout(() => this.setState({ redirect: true }), 3000);
		this.interval = setInterval(() => this.setState({ count: this.state.count -1 }), 1000);
	  }
	  componentWillUnmount() {
		clearInterval(this.interval);
	  }
	
	render(){
		if(this.props.location.state !== undefined){
			if(this.state.redirect){
				this.props.history.push({
					pathname: '/user/course-info/MQ==',
					state: { new:true}
				}
			   )
			}
		}else{
			this.props.history.push('/')
		}
		return(
			<>
			<Helmet>
                <meta charSet="utf-8" />
                <title>Registration success.</title>
                <meta name="description" content="Registration success." />
            </Helmet>
			<div className="t3-wrapper">
			<Header/>
				<main>
				<section class="sec">
				<div class="container">
					<div class="row">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class="thankyou-div">
							<img src={require("../../assets/images/correct-green.png")} class="img-fluid"/>
							<h3>Thank You!</h3>
							<h6>Registration successfully</h6>
							<p>Please check your email for login details.</p>
							<div class="backdiv">
							<h3>Redirect in {this.state.count}</h3>
							</div>
						</div>
					</div>
					</div>
				</div>
				</section>  
			</main>
		    <Footer/>
			</div>
			</>
			);
	}
}

export default RegistrationSuccess;