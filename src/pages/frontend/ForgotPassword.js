import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {Helmet} from "react-helmet";
import AuthService from '../../services/authServices';
import SimpleReactValidator from 'simple-react-validator';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';

class ForgotPassword extends Component {

	constructor(props) {
		super(props);
		this.validator = new SimpleReactValidator();
		const max = 9;
		const rand1 = Math.floor(Math.random(1) * Math.floor(max));
		const rand2 = Math.floor(Math.random(1) * Math.floor(max));
		this.state = {
			email: '',
			isloading:false,
			showError: false,
			errorMessage: '',
			showSuccess: false,
			successMessage: '',
			securityVal: '',
			security: true,
			rand1: rand1,
			rand2: rand2,
			currectSecurity: rand1 + rand2,
		}
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.resetForm = this.resetForm.bind(this);
		this.onRecaptchaChange = this.onRecaptchaChange.bind(this);
	}

	componentDidMount() {
		window.scrollTo(0, 0);
	}

	onChange(e){
		this.setState({ [e.target.name] : e.target.value });
	}

	onRecaptchaChange(e){
		this.setState({  securityVal: e.target.value });
		let currentVal = Number(e.target.value);
		const currectVal = this.state.currectSecurity;
		if(currentVal !== currectVal){
		  this.setState({security: false});
		}else{
		   this.setState({security: true});
		}
	  }

	onSubmit(e) {
		e.preventDefault();
		this.setState({ showError: false });
		this.setState({ showSuccess: false });
		if (!this.validator.allValid()) {
			this.validator.showMessages();
			this.forceUpdate();
			return false;
		}
		if (!this.state.security) {
			return false;
		 }
		 this.setState({ isloading: true });
		AuthService.forgotPasswordMail(this.state.email)
		.then(res => {
			this.resetForm();
			this.setState({ showError: false, isloading: false });
			this.setState({ errorMessage: '' });
			this.setState({ showSuccess: true });
			this.setState({ successMessage: res.data.message });
		})
		.catch(error => {
			this.setState({ showSuccess: false, isloading: false });
			this.setState({ successMessage: '' });
			this.setState({ showError: true });
			this.setState({ errorMessage: error.response.data.message });
		});
		
	}

	resetForm(){
		this.validator.hideMessages();
		document.getElementById("changePasswordForm").reset();
		this.setState({
			email : '',
			securityVal: '',
			security: true,
		});
	  }
	render() {
		const { isloading } = this.state;
		return (
			<>
			 <Helmet>
                <meta charSet="utf-8" />
                <title>Forgot Password.</title>
            </Helmet>
				{isloading &&
					<div className="preloader-background">
						<div class="big sattva_loader active">
							<img src={require('../../assets/images/loader.png')} />
						</div>
					</div>
				}
				<div className="t3-wrapper">
					<Header />
					<main>
						<div className="sec">
							<div className="container">
								<div className="row justify-content-center">
									<div className="col-md-7 col-sm-12">
										{alert.message && alert.type === 'error' &&
											<div className="alert alert-danger alert-dismissible fade show" role="alert">
												{alert.message}

											</div>}
										{alert.message && alert.type === 'success' &&
											<div className="alert alert-success alert-dismissible fade show" role="alert">
												{alert.message}

											</div>}
										{this.state.showError === true &&
												<div className="alert alert-danger alert-dismissible fade show" role="alert">
													<p>{this.state.errorMessage}</p>
												</div>}
										{this.state.showSuccess === true &&
												<div className="alert alert-success alert-dismissible fade show" role="alert">
													<p>{this.state.successMessage}</p>
												</div>}
										<div className="card subscription-card customer-support">
											<form autocomplete="off" id="changePasswordForm" className="form form-horizontal" onSubmit={this.onSubmit}>
												<h4>Forgot Password</h4>
												<div className="">
													<div className="input-field">
														<label id="email-lbl" for="email">Email Address<span className="required">&#160;*</span></label>
														<input type="email" name="email" id="email" aria-required="true" value={this.state.email} onChange={this.onChange} />
														{this.validator.message('email', this.state.email, 'required|email')}
													</div>
												</div>
												<div className="row">
												<div className="input-field col-md-12">
													<div className="security-check">
														<div className="input-group-prepend">
															<span>{this.state.rand1} + {this.state.rand2}</span>
														</div>
														<div className="security-addon">
															<label id="security-lbl" for="security">Security Check <span className="required">&nbsp;*</span></label>
															<input type="number" name="securityVal" id="security" value={this.state.securityVal} onChange={this.onRecaptchaChange} />
															{this.validator.message('Recaptcha', this.state.securityVal, 'required')}
														</div>
													</div>
												</div>
												<div className="input-field col-md-12">
														{this.state.security === false &&
														<div className="alert alert-danger alert-dismissible fade show resetpass-error" role="alert">
															<p>Incorrect Recaptcha</p>
														</div>}	
												</div>
												</div>
												<div className="">
													<div className="input-field s12">
														<button type="submit" className="btn btn-lg">Submit</button>
													</div>
												</div>
											</form>
											<br/>
											<div className="login-links">
												<div className="input-field">
													<Link to={window.PUBLIC_URL + '/login'}>Back to Login.</Link>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</main>
					<Footer />
				</div>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	alert: state.alert,
});
export default connect(mapStateToProps, {})(ForgotPassword);