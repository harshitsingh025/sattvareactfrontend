import React from 'react';
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';

export default function UserRegistrationSuccess() {
    return (
        <div className="t3-wrapper">
            <Header />
            <main>
                <section className="sec sec-cinfo">
                    <div className="container">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <div class="thankyou-div">
                                    <img src={require("../../assets/images/correct-green.png")} class="img-fluid" />
                                    <h3>Thank You!</h3>
                                    <h6>Registration successfully</h6>
                                    <p>Please check your email for login details.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
            <Footer />
        </div>
    )
}
