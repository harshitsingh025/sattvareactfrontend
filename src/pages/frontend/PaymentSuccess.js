import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import Header from '../../components/user/Header';
import AOS from 'aos';
import { Helmet } from "react-helmet";
import Footer from '../../components/frontend/common/Footer';
import UserServices from '../../services/userServices';

class PaymentSuccess extends Component {
	state = {
		redirect: false,
		count: 3,
		coursePurchased: false,
	}
	componentDidMount() {
		AOS.init();
		window.scrollTo(0, 0);
		const user = JSON.parse(localStorage.getItem('auth'));
		if (user) {
			UserServices.getUserInformation(btoa(user.userDetails.id))
				.then(res => {
					this.setState({ coursePurchased: res.data.coursePurchased });
				})
				.catch(error => {
					this.setState({ coursePurchased: false });
				});
		}

		setTimeout(() => this.setState({ redirect: true }), 3000);
		this.interval = setInterval(() => this.setState({ count: this.state.count - 1 }), 1000);
	}
	componentWillUnmount() {
		clearInterval(this.interval);
	}

	render() {
		if (this.state.coursePurchased) {
			let userDetails = JSON.parse(localStorage.getItem('auth'));
			userDetails.userType = 1;
			localStorage.setItem('auth', JSON.stringify(userDetails));
		}
		if (this.state.redirect) {
			if (this.state.coursePurchased) {
				return (
					<Redirect to={window.PUBLIC_URL + '/user/course-details/MQ=='} />
				);
			} else {
				return (
					<Redirect to={window.PUBLIC_URL + '/user/course-info/MQ=='} />
				);
			}

		}
		return (
			<>
				<Helmet>
					<meta charSet="utf-8" />
					<title>Course payment success.</title>
					<meta name="description" content="Course payment success." />
				</Helmet>
				<Header />
				<main class="admin-content">
					<section class="sec">
						<div class="container">
							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="thankyou-div">
										<img src={require("../../assets/images/correct-green.png")} class="img-fluid" />
										<h3>Thank You!</h3>
										<h6>Payment successfully</h6>
										<p>Thank you for purchasing our course.</p>
										<div class="backdiv">
											<h3>Redirect in {this.state.count}</h3>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</main>
				<Footer />
			</>
		);
	}
}

export default PaymentSuccess;