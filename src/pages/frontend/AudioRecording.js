import React from 'react';
import {Helmet} from "react-helmet";
import Header from '../../components/frontend/common/Header';
import Footer from '../../components/frontend/common/Footer';

export default function AudioRecording() {
    return (
        <>
        <Helmet>
                <meta charSet="utf-8" />
                <title>Audio Recordings.</title>
            </Helmet>
            <Header />
            <main>
                <section class="sec sec-inabout">
                <div className="container">
                    <h4>Audio Recordings</h4>
                    <p>Very soon, we will share a selection of our audio recordings from our wisdom library with you. Stay tuned.</p>
                    <p>The teachings in these audio recordings have the potential to transform your life into one that is a declaration of love, joy, and peace!</p>
                </div>
                </section>  
            </main>
            <Footer />
        </>
    )
}
