import React,{Component} from 'react';
import { connect } from 'react-redux';
import AOS from 'aos';
import {Helmet} from "react-helmet";
import Footer from '../../components/frontend/common/Footer';
import Header from '../../components/frontend/common/Header';
import ContactUsForm from '../../components/frontend/contactus/ContactUsForm';

class Contactus extends Component{


	componentDidMount() {
	window.scrollTo(0, 0);
	AOS.init();
	}

	render(){
		return(
			<>
			<Helmet>
                <meta charSet="utf-8" />
                <title>Connect with us | Unlimited access to yoga classes online.</title>
				<meta name="description" content="Practice with Sattva Connect today! Find ease and peace, greater joy and love. Sattva Connect is Your Support to an Awakened Life." />
				<meta name="keywords" content="sattva yoga online, sattva yoga, sattva yoga journey online, yoga and meditation" />
				<link rel="canonical" href="https://www.sattvaconnect.com/contact-us/"/>
			</Helmet>
			<Header/>
			<ContactUsForm/>
			<Footer/>
			</>
			);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(Contactus);