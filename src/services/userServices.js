import axios from 'axios';
import { apiRoute, getApiHeader } from '../utils/helpers';

class UserServices {

    static sendContactMail(data) {
        const requestOptions = {
            headers: getApiHeader(null, true),
        };

        const body = JSON.stringify(data);

        return axios.post(apiRoute('user/send-user-mail'), body, requestOptions);
    }

    static checkUsernameAvailability(username, OldUsername){

		const requestOptions = {
			headers : getApiHeader(null, true)
		};

		return axios.get(apiRoute('user/check-username-availability/'+username+'/'+OldUsername),requestOptions)
	}

	static checkEmailAvailability(email, oldEmail){

		const requestOptions = {
			headers : getApiHeader(null, true)
		};

		return axios.get(apiRoute('user/check-email-availability/'+email+'/'+oldEmail),requestOptions)
	}

	static updateUserDetails(data) {
        const requestOptions = {
            headers: getApiHeader(null, true),
        };

        const body = JSON.stringify(data);

        return axios.post(apiRoute('user/update-user-detail'), body, requestOptions);
    }

    static changeUserPassword(data) {
        const requestOptions = {
            headers: getApiHeader(null, true),
        };

        const body = JSON.stringify(data);

        return axios.post(apiRoute('user/change-user-password'), body, requestOptions);
    }

     static deleteUserAccount(id) {
        const requestOptions = {
            headers: getApiHeader(null, true),
        };

        return axios.get(apiRoute('user/delete-user/'+id), requestOptions);
    }

    static buyNewCourse(data) {
        const requestOptions = {
            headers: getApiHeader(null, true),
        };

        const body = JSON.stringify(data);

        return axios.post(apiRoute('user/buy-new-course'), body, requestOptions);
    }

    static sendForgotPasswordMail(data) {
        const requestOptions = {
            headers: getApiHeader(null, false),
        };

        const body = JSON.stringify(data);

        return axios.post(apiRoute('user/send-forgot-password-mail'), body, requestOptions);
    }

    static sendCustomerSupportMail(data) {
        const requestOptions = {
            headers: getApiHeader(null, false),
        };

        const body = JSON.stringify(data);

        return axios.post(apiRoute('send-customer-support-mail'), body, requestOptions);
    }

    static getUserInformation(id) {
        const requestOptions = {
			headers : getApiHeader(null, true)
		};
		return axios.get(apiRoute('get-user-information/'+id),requestOptions)
    }
}

export default UserServices;
