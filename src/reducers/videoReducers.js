import * as types from '../actions/types';
import initialState from './initialState'; 

export default function alert(state = initialState.video, action){
	 switch(action.type){

	 	case types.FETCH_VIDEOS:
	 	return {
	 		...state,
	 		items: action.videos,
			item: {},
			hasMore: action.hasMore,
			cursor:action.cursor,
			totalCount:action.totalCount,
		 };
		case types.VIDEO_FILTER:
	 	return {
	 		...state,
	 		searchState: action.filter,
	 	};
	 	default:
	 	return state;	
	 }
}

