import * as types from '../actions/types';
import initialState from './initialState'; 

export default function(state = initialState.teacherDetails, action){
	 switch(action.type){

	 	case types.FETCH_TEACHER_DETAILS:
	 	return {
	 		...state,
	 		item: action.payload
	 	}
	 	default:
	 	return state;	
	 }
}

