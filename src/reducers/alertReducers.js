import * as types from '../actions/types';
import initialState from './initialState'; 

export default function(state = initialState.alert,{ type, message, ...payload}){
	 switch(type){

	 	case types.ALERT_SUCCESS:
	 	return {
	 		isloading: false,
	        type: 'success',
	        message: message,
	        ...payload
	 	};

	 	case types.ALERT_ERROR:
	 	return {
	 		isloading: false,
	        type: 'error',
	        message: message,
	        ...payload
		 	};

		case types.ALERT_CLEAR:
	      return {
	        ...initialState.alert,
       		...payload,
	      };

	 	default:
	 	return state;	
	 }
}

