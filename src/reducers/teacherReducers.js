import * as types from '../actions/types';
import initialState from './initialState'; 

export default function(state = initialState.teacher, action){
	 switch(action.type){

	 	case types.FETCH_TEACHER:
	 	return {
	 		...state,
	 		items: action.payload
	 	}
	 	default:
	 	return state;	
	 }
}

