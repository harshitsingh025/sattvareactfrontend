import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import teacherDetailsReducers from './teacherDetailsReducers';
import teamReducers from './teamReducers';
import teacherReducers from './teacherReducers';
import courseReducers from './courseReducers';
import alertReducers from './alertReducers';
import userReducers from './userReducers';
import authReducers from './authReducers';
import streamReducers from './streamReducers';
import videoReducers from './videoReducers';

export default (history) => combineReducers({
  router: connectRouter(history),
  teams: teamReducers,
  teachers: teacherReducers,
  courses: courseReducers,
  teacherDetails: teacherDetailsReducers,
  alert: alertReducers,
  userDetails: userReducers,
  auth:authReducers,
  stream:streamReducers,
  video:videoReducers,
})