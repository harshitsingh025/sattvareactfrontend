import * as types from '../actions/types';
import initialState from './initialState'; 

export default function alert(state = initialState.userDetails, action){
	 switch(action.type){

	 	case types.STORE_USER_DETAILS:
	 	return {
	 		...state,
	 		item: action.payload
	 	};
	 	case types.FETCH_USER_COURSE:
	 	return {
	 		...state,
			 courses: action.courses,
			 hasMore: action.hasMore,
			 cursor:action.cursor,
	 	};
	 	case types.FETCH_USER_AVAILABLE_COURSE:
	 	return {
	 		...state,
			 availableCourses: action.courses,
			 hasMore: action.hasMore,
			 cursor:action.cursor,
	 	};
	 	case types.FETCH_RELATED_COURSES:
	 	return {
	 		...state,
	 		relatedCourses: action.payload
	 	};
	 	case types.USER_UPDATE_SUCCESS:
	 	return {
	 		...state,
	 		item: action.payload,
		 };
		case types.COURSE_PURCHASED_OR_NOT:
	 	return {
	 		...state,
	 		coursePurchased: action.status,
	 	};
	 	default:
	 	return state;	
	 }
}

