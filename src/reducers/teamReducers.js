import * as types from '../actions/types';
import initialState from './initialState'; 

export default function(state = initialState.team, action){
	 switch(action.type){

	 	case types.FETCH_TEAM:
	 	return {
	 		...state,
	 		items: action.payload
	 	}
	 	default:
	 	return state;	
	 }
}

