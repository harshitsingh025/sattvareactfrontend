import * as types from '../actions/types';
import initialState from './initialState'; 

export default function(state = initialState.livestream, action){
	 switch(action.type){

	 	case types.LIVE_CHAT_MESSAGES:
	 	return {
	 		...state,
	 		messages: action.payload
	 	}
	 	default:
	 	return state;	
	 }
}

