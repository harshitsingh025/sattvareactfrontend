import * as types from '../actions/types';
import initialState from './initialState';

export default function auth(state = initialState.auth, { type, ...payload }) {
  switch (type) {
    case types.LOGIN_REQUEST:
      return {
        isAuthenticated: false,
        ...payload
      };
    case types.LOGIN_SUCCESS:
      return {
        isAuthenticated: true,
        ...payload
      };
    case types.LOGIN_FAILED:
      return {
        isAuthenticated: false,
        ...payload
      };
    case types.LOGOUT:
      return {
        isAuthenticated: false,
        ...payload
      };
    default:
      return state;
  }
}