import * as types from '../actions/types';
import initialState from './initialState'; 

export default function alert(state = initialState.course, action){
	 switch(action.type){

	 	case types.FETCH_COURSE:
	 	return {
	 		...state,
	 		items: action.courses,
			item: {},
			hasMore: action.hasMore,
			cursor:action.cursor,
	 	}
	 	case types.FETCH_COURSE_DETAILS:
	 	return {
	 		...state,
	 		item: action.payload
	 	}
	 	case types.FETCH_BRAINTREE_CLINT_ID:
	 	return {
	 		...state,
	 		braintree: action.payload
	 	}
	 	case types.FETCH_COURSE_ALL_DETAILS:
	 	return {
	 		...state,
	 		allDetails: action.payload
	 	}
	 	default:
	 	return state;	
	 }
}

