import { getLocalStorageAuth } from '../utils/helpers';
import { MessageLivestream } from 'stream-chat-react';

const authData = getLocalStorageAuth();

export default 
{
   alert: 
   {
        isloading: false,
        status: '',
        type: '',
        message: '',
        errors: [],
        redirect: false,
    },
    auth: {
        isAuthenticated: authData ? true : false,
        ...authData
    },
    teacher: 
    {
        items: [],
    },
    team:
    {
	    items: [],
    },
    course:
    {
        items: [],
        item: {},
        allDetails:{},
        braintree: '',
        cursor:0,
        hasMore:true,
    },
    teacherDetails:
    {
        item: [],
    },
    userDetails:{
        item:{},
        courses:[],
        availableCourses:[],
        relatedCourses:[],
        cursor:0,
        hasMore:true,
        coursePurchased:false,
    },
    livestream:{
        messages:[]
    },
    video:{
        items: [],
        item: {},
        allDetails:{},
        cursor:0,
        hasMore:false,
        totalCount:0,
        searchState:{
            videos: [],
            teachers: [],
            styleType: [],
            intentions: [],
            styles: [],
            searchInput: '',
            duration: '',
            durationStart:'',
            selectedTeacher: '',
            selectedStyle: '',
            selectedStyleType: '',
            selectedTeacherText: '',
            selectedStyleText: '',
            selectedStyleTypeText: '',
            selectedIntention: [],
            searchInputOptions:[],
            showFilterForm: false,
            showStyle: false,
          }
    }
}