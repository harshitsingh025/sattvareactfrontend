import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import SimpleReactValidator from 'simple-react-validator';
import { setLoadingTrue, clearAllAlerts } from '../../actions/userAction';
import AuthService from '../../services/authServices';
import CountryOptions from '../frontend/common/CountryOptions';
import { getLocalStorageAuth, setLocalStorageAuth } from '../../utils/helpers';
import { apiRoute, getApiHeader, userProfilePath } from '../../utils/helpers';

class UserDetailsForm extends Component {
    constructor(props) {
        super(props);
        this.usernameValidate = new SimpleReactValidator();
        this.emailValidate = new SimpleReactValidator();
        this.changePasswordValidate = new SimpleReactValidator();
        this.userDetailsValidate = new SimpleReactValidator();
        this.state = {
            username: '',
            email: '',
            oldEmail: '',
            oldUsername: '',
            userId: '',
            first_name: '',
            last_name: '',
            phone: '',
            address1: '',
            address2: '',
            city: '',
            zip: '',
            country: '',
            alert: false,
            alertType: '',
            alertMsg: '',
            password: '',
            profilePic: '',
            image: '',
            countryFlag:'',
            countries:[],
            timezones:[],
            timezone:'',
            confirmPassword: '',
            emailAvailability: true,
            usernameAvailability: true,
            passwordStatus: true,
        }
        this.onChange = this.onChange.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.handleUsernameForm = this.handleUsernameForm.bind(this);
        this.handleUserEmailForm = this.handleUserEmailForm.bind(this);
        this.handleChangePasswordForm = this.handleChangePasswordForm.bind(this);
        this.handleUserDetailsForm = this.handleUserDetailsForm.bind(this);
    };

    componentWillMount() {

        window.scrollTo(0, 0);

        const authData = getLocalStorageAuth();
        if(authData){
        const userDetails = authData.userDetails;

        this.setState({
            userId: userDetails.id,
            email: userDetails.email,
            oldEmail: userDetails.email,
            username: userDetails.username,
            oldUsername: userDetails.username,
            first_name: userDetails.first_name,
            last_name: userDetails.last_name,
            phone: userDetails.phone,
            city: userDetails.city,
            address1: userDetails.address1,
            address2: userDetails.address2,
            zip: userDetails.zip_code,
            countryCode: userDetails.country_code,
            country: userDetails.country,
            profilePic: userDetails.profile_pic,
            timezone: userDetails.timezone_id,
        });
        const requestOptions = {
			headers : getApiHeader()
        };
        
        axios.get(apiRoute('get-all-countries'),requestOptions)
		.then(res =>{
            this.setState({ countries : res.data.countries });
            const result = res.data.countries.filter(item => item.country == userDetails.country);
            if(result[0].CountryCode){
                const flag =  result[0].CountryCode.toLowerCase();
                this.setState({ countryFlag : flag});
            }
           
        });

        axios.get(apiRoute('get-all-timezone'),requestOptions)
		.then(res =>{
            this.setState({ timezones : res.data });
        });
    }}

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    onUsernameChange(e) {
        const userName = e.target.value;
        this.setState({ username: userName });
        AuthService.checkUsernameAvailability(userName, this.state.oldUsername)
            .then(res => {
                if (res.data.status !== true) {
                    this.setState({ usernameAvailability: false });
                } else {
                    this.setState({ usernameAvailability: true });
                }
            })
    }

    onEmailChange(e) {
        const email = e.target.value;
        this.setState({ email: email });
        AuthService.checkEmailAvailability(email, this.state.oldEmail)
            .then(res => {
                if (res.data.status.status !== true) {
                    this.setState({ emailAvailability: false });
                } else {
                    this.setState({ emailAvailability: true });
                }
            })

    }

    handleImageChange = (e) => {
        this.setState({
            image: e.target.files[0],
        });
    }

    handleUsernameForm(e) {
        e.preventDefault();
        if (!this.usernameValidate.allValid()) {
            this.usernameValidate.showMessages();
            this.forceUpdate();
            return false;
        }
        this.props.setLoadingTrue();
        const userUsername = {
            id: this.state.userId,
            username: this.state.username,
        }
        AuthService.updateUserUsername(userUsername)
            .then(res => {
                window.scrollTo(0, 0);
                const auth = getLocalStorageAuth();
                auth.userDetails.username = this.state.username;
                setLocalStorageAuth(auth);
                this.setState({ alert: true, alertType: 'success', alertMsg: res.data.message });
                this.props.clearAllAlerts();
            })
            .catch(error => {
                window.scrollTo(0, 0);
                this.setState({ alert: true, alertType: 'error', alertMsg: 'Something went wrong please try again.' });
                this.props.clearAllAlerts();
            });
    }

    handleUserEmailForm(e) {
        e.preventDefault();
        if (!this.emailValidate.allValid()) {
            this.emailValidate.showMessages();
            this.forceUpdate();
            return false;
        }
        this.props.setLoadingTrue();
        const userEmail = {
            id: this.state.userId,
            email: this.state.email,
        }
        AuthService.updateUserEmail(userEmail)
            .then(res => {
                window.scrollTo(0, 0);
                const auth = getLocalStorageAuth();
                auth.userDetails.email = this.state.email;
                setLocalStorageAuth(auth);
                this.setState({ alert: true, alertType: 'success', alertMsg: res.data.message });
                this.props.clearAllAlerts();
            })
            .catch(error => {
                window.scrollTo(0, 0);
                this.setState({ alert: true, alertType: 'error', alertMsg: 'Something went wrong please try again.' });
                this.props.clearAllAlerts();
            });
    }

    handleChangePasswordForm(e) {
        e.preventDefault();
        if (!this.changePasswordValidate.allValid()) {
            this.changePasswordValidate.showMessages();
            this.forceUpdate();
            return false;
        }
        if (this.state.password !== this.state.confirmPassword) {
            window.scrollTo(0, 0);
            this.setState({ passwordStatus: false });
            return false;
        } else {
            this.setState({ passwordStatus: true });
        }
        this.props.setLoadingTrue();
        const userDetails = {
            id: this.state.userId,
            password: this.state.password,
        }
        AuthService.changePassword(userDetails)
            .then(res => {
                window.scrollTo(0, 0);
                this.setState({ alert: true, alertType: 'success', alertMsg: res.data.message, confirmPassword: '', password: '' });
                this.props.clearAllAlerts();
            })
            .catch(error => {
                window.scrollTo(0, 0);
                this.setState({ alert: true, alertType: 'error', alertMsg: 'Something went wrong please try again.' });
                this.props.clearAllAlerts();
            });

    }

    handleUserDetailsForm(e) {
        e.preventDefault();
        if (!this.userDetailsValidate.allValid()) {
            this.userDetailsValidate.showMessages();
            this.forceUpdate();
            return false;
        }
        let data = new FormData();
        data.append('image', this.state.image);
        data.append('id', this.state.userId);
        data.append('first_name', this.state.first_name);
        data.append('last_name', this.state.last_name);
        data.append('phone', this.state.phone);
        data.append('city', this.state.city);
        data.append('address1', this.state.address1);
        data.append('address2', this.state.address2);
        data.append('zip_code', this.state.zip);
        data.append('country_code', this.state.countryCode);
        data.append('country', this.state.country);
        data.append('timezone', this.state.timezone);
        this.props.setLoadingTrue();

        AuthService.updateUserDetails(data)
            .then(res => {
                window.scrollTo(0, 0);
                const auth = getLocalStorageAuth();
                auth.userDetails = res.data.user;
                setLocalStorageAuth(auth);
                this.setState({ alert: true, alertType: 'success', alertMsg: res.data.message, profilePic: res.data.user.profile_pic });
                const result = this.state.countries.filter(item => item.country == this.state.country);
                if(result[0].CountryCode){
                    const flag =  result[0].CountryCode.toLowerCase();
                    this.setState({ countryFlag : flag});
                }
                this.props.clearAllAlerts();
            })
            .catch(error => {
                window.scrollTo(0, 0);
                this.setState({ alert: true, alertType: 'error', alertMsg: 'Something went wrong please try again.' });
                this.props.clearAllAlerts();
            });
    }

    onCountryChange = (e) => {
        const index = e.target.selectedIndex;
        const optionElement = e.target.childNodes[index]
        const phoneCode = optionElement.getAttribute('phoneCode');
        this.setState({ country: e.target.value });
        this.setState({ countryCode: phoneCode });
    }

    render() {
        const { alert, alertType, alertMsg } = this.state;
        return (
            <>
                {alert && alertType === 'error' &&
                    <div className="alert alert-danger" role="alert">
                        {alertMsg}
                    </div>}
                {alert && alertType === 'success' &&
                    <div className="alert alert-success" role="alert">
                        {alertMsg}
                    </div>}
                {this.state.passwordStatus === false &&
                    <div className="alert alert-danger" role="alert">
                        <p>Password does not match</p>
                    </div>}
                <div className="row">
                    <div className="col-md-6">
                        <form onSubmit={this.handleUserDetailsForm}>
                            <h6 className="card-title">Profile Information</h6>
                            <div className="upload-file">
                                <div className="upload-img">
                                    <img className="responsive-img" src={userProfilePath(this.state.profilePic)} />
                                </div>
                                <div className="upload-btn">
                                    <input type="file" onChange={this.handleImageChange} />
                                    <i className="fas fa-camera"></i>
                                </div>
                            </div>
                            <div className="profile-inputs">
                                <div className="row">
                                    <div className="input-field col-md-6">
                                        <i className="fas fa-user-circle"></i>
                                        <input id="first_name" value={this.state.first_name} name="first_name" type="text" className="pinf" onChange={this.onChange} />
                                        {this.userDetailsValidate.message('first_name', this.state.first_name, 'required')}
                                        <label for="first_name" className='active'>First Name*</label>
                                    </div>
                                    <div className="input-field col-md-6">
                                        <i className="fas fa-user-circle"></i>
                                        <input id="last_name" value={this.state.last_name} name="last_name" type="text" className="pinf" onChange={this.onChange} />
                                        {this.userDetailsValidate.message('last_name', this.state.last_name, 'required')}
                                        <label for="last_name" className='active'>Last Name*</label>
                                    </div>
                                </div>
                            </div>
                            <div className="profile-inputs">
                                <div className="row">
                                    <div className="input-field select-field col-md-6">
                                        <i className="fas fa-globe"></i>
                                        <label className='active'>Country*</label>
                                        <select id="country" name="country" onChange={this.onCountryChange}>
                                            <option>Country *</option>
                                            {this.state.countries.map((item,index)=>{
                                                const selected = this.state.country === item.country ? true:false;
                                                return(
                                                <option  phonecode={item.phonecode} key={index} value={item.country} selected={selected}>{item.country}</option>
                                                );
                                            })}	
                                        </select>
                                        {this.userDetailsValidate.message('country', this.state.country, 'required')}
                                    </div>
                                    <div className="input-field select-field col-md-4">
                                           {this.state.countryFlag !== '' &&
                                             <img src={'https://betasite.online/sattvaconnect/public/images/flags/'+this.state.countryFlag+'.png'} height='35'/>
                                           }
                                    </div>
                                </div>
                            </div>
                            <div className="profile-inputs">
                                <div className="row">
                                    <div class="input-field col-md-4 no-icons pl-0">
                                        <input id="custom_osm_dial_code" name="countryCode" type="text" value={this.state.countryCode} className="pinfsm" onChange={this.onChange} />
                                        {this.userDetailsValidate.message('countryCode', this.state.countryCode, 'required|integer')}
                                    </div>
                                    <div className="input-field col-md-8">
                                        <input id="phone" name="phone" type="text" value={this.state.phone} className="pinf" onChange={this.onChange} />
                                        {this.userDetailsValidate.message('phone', this.state.phone, 'required|max:17')}
                                        <label for="phone" className='active'>Phone(+)*</label>
                                    </div>
                                    <div className="input-field col-md-12">
                                        <i className="fas fa-address-book"></i>
                                        <input id="address" name="address1" className="materialize-textarea pinf" value={this.state.address1} onChange={this.onChange} />
                                        {this.userDetailsValidate.message('address1', this.state.address1, 'required')}
                                        <label for="address" className="active">Address*</label>
                                    </div>
                                    <div className="input-field col-md-12">
                                        <i className="fas fa-address-book"></i>
                                        <input id="address2" name="address2" className="materialize-textarea pinf" value={this.state.address2 !== 'null' ? this.state.address2 : ''} onChange={this.onChange} />
                                        <label for="address2" className="active">Address2</label>
                                    </div>
                                    <div className="input-field col-md-6">
                                        <i className="fas fa-building"></i>
                                        <input id="city" value={this.state.city} name="city" type="text" className="pinf" onChange={this.onChange} />
                                        {this.userDetailsValidate.message('city', this.state.city, 'required')}
                                        <label for="city" className='active'>City*</label>
                                    </div>
                                    <div className="input-field col-md-6">
                                        <i class="fa fa-caret-square-o-right" aria-hidden="true"></i>
                                        <input id="zip" name="zip" type="text" value={this.state.zip} className="pinf" onChange={this.onChange} />
                                        {this.userDetailsValidate.message('zip', this.state.zip, 'required|max:7')}
                                        <label for="zip" className='active'>Zip*</label>
                                    </div>
                                    
                                </div>
                            </div>
                            <div className="profile-inputs">
                                <div className="row">
                                    <div className="input-field select-field col-md-12">
                                        <i className="fas fa-map-marker-alt"></i>
                                        <label className='active'>Timezone*</label>
                                        <select id="timezone" name="timezone" onChange={this.onChange}>
                                            <option>Timezone </option>
                                            {this.state.timezones.map((item,index)=>{
                                                const selected = this.state.timezone == item.id ? true:false;
                                                return(
                                                <option key={index} value={item.id} selected={selected}>{item.timezone}</option>
                                                );
                                            })}	
                                        </select>
                                        {this.userDetailsValidate.message('timezone', this.state.timezone, 'required')}
                                    </div>
                                </div>
                            </div>
                            <div className="text-right">
                            <button title="Update Profile" className="btn btn-sm">Update Profile <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>
                    <div className="col-md-6 mt-md-0 mt-4">
                        <h6 className="card-title">Change login details</h6>
                        <form id='usernameForm' onSubmit={this.handleUsernameForm}>
                            {this.state.usernameAvailability === false &&
                                <div className="col-sm-8"><div className="alert alert-danger alert-dismissible fade show" role="alert">
                                    <p>Username is already in used.</p>
                                </div></div>}
                            <div className="profile-inputs">
                                <div className="input-field">
                                    <div className="row">
                                        <div className="col-sm-9">
                                            <i className="fas fa-user-circle active"></i>
                                            <input id="username" autoComplete="new-username" name="username" value={this.state.username} className="pinf" onChange={this.onUsernameChange} />
                                            {this.usernameValidate.message('username', this.state.username, 'required')}
                                        </div>
                                        <div className="col-sm-3">
                                            <button className="btn  btn-sm" type='submit'>Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id='userEmailForm' onSubmit={this.handleUserEmailForm}>
                            {this.state.emailAvailability === false &&
                                <div className="col-sm-8">
                                    <div className="alert alert-danger alert-dismissible fade show" role="alert">
                                        <p>Email is already in used.</p>
                                    </div></div>}
                            <div className="profile-inputs">
                                <div className="input-field">
                                    <div className="row">
                                        <div className="col-sm-9">
                                            <i className="fas fa-envelope"></i>
                                            <input id="uemail" name="uemail" value={this.state.email} className="pinf" onChange={this.onEmailChange} />
                                            {this.emailValidate.message('username', this.state.email, 'required')}
                                        </div>
                                        <div className="col-sm-3"><button className="btn  btn-sm" type='submit'>Save</button></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id='usernameFormbottom' onSubmit={this.handleChangePasswordForm}>
                            <div className="profile-inputs">
                                <div className="input-field">
                                    <div className="row">
                                        <div className="col-sm-9">
                                            <i className="fas fa-unlock-alt"></i>
                                            <input autoComplete="new-password" name='password' value={this.state.password} id="password" type="password" className="pinf" onChange={this.onChange} />
                                            {this.changePasswordValidate.message('password', this.state.password, 'required')}
                                            <label for="password">Password*</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="profile-inputs">
                                <div className="input-field">
                                    <div className="row">
                                        <div className="col-sm-9">
                                            <i className="fas fa-unlock-alt"></i>
                                            <input id="password1" type="password" name='confirmPassword' className="pinf" value={this.state.confirmPassword} onChange={this.onChange} />
                                            {this.changePasswordValidate.message('confirmPassword', this.state.confirmPassword, 'required')}
                                            <label for="email">Confirm password*</label>
                                        </div>
                                        <div className="col-sm-3"><button className="btn  btn-sm" type='submit'>Save</button></div>
                                    </div>
                                </div>
                            </div>                            
                        </form>
                    </div></div>
            </>
        );
    }
}
const mapStateToProps = (state) => ({
    alert: state.alert,
});

export default connect(mapStateToProps, { setLoadingTrue, clearAllAlerts })(UserDetailsForm);