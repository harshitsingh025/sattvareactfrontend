import React,{Component} from 'react';
import { connect } from 'react-redux';
import cookie from 'react-cookies';
import { Link, Redirect,NavLink,useLocation  } from 'react-router-dom';
import { getLocalStorageAuth, removeLocalStorageAuth } from '../../../utils/helpers';

class Header extends Component{
	constructor(props) {
		super(props);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		const userData = getLocalStorageAuth();
       
		if (!userData) {
            this.state = {
                redirect: true,
            } 
			
		} else {
            const auth = cookie.load('auth');
            if(auth !== undefined){
                this.state = {
                    redirect: false,
                }
            }else{
                removeLocalStorageAuth();
                this.state = {
                    redirect: true,
                  
                } 
            }
        }
      }

    onDeleteClick() {
		localStorage.removeItem('auth');
		this.setState({
			redirect: true,
		});
    }
    
	render(){
        const { alert } = this.props;
        if (this.state.redirect) {
			return (
				<Redirect to={window.PUBLIC_URL + '/login'} />
			);
		}
			return(
			<>
            	{alert && alert.isloading &&
						<div className="preloader-background">
							<div className="big sattva_loader active">
								<img src={require('../../../assets/images/loader.png')} />
							</div>
						</div>
					}
                    <header className="sidebar-head user-header">
                        <img className="user-logo img-fluid" src={require('../../../assets/images/logo-sattva.png')} alt="Sattva Connect" />
						
                    <button type="button" id="sidebarCollapse" className="btn btn-toggle">
                    <i className="fas fa-bars"></i>
                    </button>
                    <nav id="sidebar" className="active">
                    <ul className="list-unstyled components">
                        
                        <li>
                        <NavLink  to="/user-dashboard/explore" activeClassName="active">
                            <i className="fas fa-eye"></i><span>Explore</span>
                        </NavLink>
                        </li>
                        <li>
                        <NavLink  to="/user-dashboard/me" activeClassName="active">
                            <i className="fas fa-user"></i><span>Me</span>
                        </NavLink>
                        </li>
                        <li>
                        <NavLink  activeClassName="active"  to="/user-dashboard/search">
                            <i className="fas fa-search"></i><span>Search</span>
                        </NavLink>
                        </li>
                        <li>
                        <NavLink to="/user-dashboard/live-stream" activeClassName="active">
                            <i className="fas fa-chalkboard-teacher"></i><span>Live Stream</span>
                        </NavLink>
                        </li>                   
                        <li>
                        <NavLink to="/user-dashboard/support" activeClassName="active">
                            <i className="fas fa-life-ring"></i><span>Support</span>
                        </NavLink>
                        </li>
                        <li>
                        <NavLink to="/user-dashboard/settings" activeClassName="active">
                            <i className="fas fa-cog"></i><span>Settings</span>
                        </NavLink>
                        </li>      
                        <li>
                        <a className=""  onClick={this.onDeleteClick}>
                            <i className="fas fa-sign-out-alt"></i><input type="button" className="btn btn-sm" name="Submit" value="Logout"/>
                        </a>
                        </li>
                    </ul>
                    </nav>
                </header>
			</>
			);
	}
}
const mapStateToProps = (state) => ({
	alert: state.alert,
});

export default connect(mapStateToProps, null)(Header);