import React,{Component} from 'react';
import { Link, NavLink } from 'react-router-dom';
import Recaptcha from 'react-google-invisible-recaptcha';

class Footer extends Component{
	
	render(){
			return(
			<>
            {/* <Recaptcha
			ref={ ref => this.recaptcha = ref }
			sitekey='6LcXXb0ZAAAAAPKkrgEa8LPjOazq4InauFR3azbD'
			onResolved={ () => console.log( 'Human detected.' ) } /> */}
			 <footer className="user-footer">
                <NavLink to="/user-dashboard/me" activeClassName="active">
                    <img src={require('../../../assets/images/home.svg')} />
                    <img src={require('../../../assets/images/homeactive.svg')} className="active"  />
                    <span>Home</span>
                </NavLink>
                <NavLink to="/user-dashboard/search" activeClassName="active">
                    <img src={require('../../../assets/images/search.svg')} />
                    <img src={require('../../../assets/images/searchactive.svg')} className="active" />
                    <span>Search</span>
                </NavLink>
                <NavLink to="/user-dashboard/explore" activeClassName="active">
                        <img src={require('../../../assets/images/send.svg')} />
                        <img src={require('../../../assets/images/sendactive.svg')} className="active"  />
                        <span>Explore</span>
                </NavLink>
                <NavLink to="/user-dashboard/live-stream" activeClassName="active">
                    <img src={require('../../../assets/images/youtube.svg')} />
                    <img src={require('../../../assets/images/youtubeactive.svg')} className="active"  />
                    <span>Live</span>
                </NavLink>
                <NavLink to="/user-dashboard/settings" activeClassName="active">
                    <img src={require('../../../assets/images/users.svg')} />
                    <img src={require('../../../assets/images/usersactive.svg')} className="active"  />
                    <span>Profile</span>
                </NavLink>
			  </footer>
		
				 </>
			);
	}
}

export default Footer;