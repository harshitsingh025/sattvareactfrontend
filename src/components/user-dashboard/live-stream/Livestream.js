import React, { Component } from 'react';
import { connect } from 'react-redux';
import {setLoadingTrue, clearAllAlerts} from '../../../actions/userAction';
import Chat from './Chat';


class LiveStream extends Component {
    constructor(props){
        super(props);
        this.state = {
            
        }
    };
  
    render() {
        return (<>
                <div className="col-md-6 broadcast-left text-center">
                    <div className="broadcast-btns">
                        <a className="btn btn-sm btn-success">Start</a>
                        <a className="btn btn-sm btn-primary">New Room</a>
                        <a className="btn btn-sm btn-danger">Quit</a>
                    </div>                        
                </div>
                <div className="col-md-2 broadcast-middle">
                    <div className="broadcast-logo my-2">
                        <img src="../images/footer_logo.png" alt=""/>
                    </div>
                    <h4 className="mb-0">Online viewers <span className="new badge">4</span></h4>
                    <p className="text-success">Saraswati</p>
                </div>
                </>
                                            
        )
    }
}
const mapStateToProps = (state) => ({
    alert: state.alert,
});

export default connect(mapStateToProps, {setLoadingTrue, clearAllAlerts })(LiveStream);
