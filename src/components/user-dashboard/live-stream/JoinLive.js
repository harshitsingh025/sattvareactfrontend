import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader, getLocalStorageAuth } from '../../../utils/helpers';
import { setLoadingTrue, clearAllAlerts } from '../../../actions/userAction';
import { OTSession, OTPublisher, OTStreams, OTSubscriber } from 'opentok-react';
import { updateChat } from '../../../actions/streamAction';
import Chat from './Chat';
import { Accordion, AccordionItem, AccordionItemHeading, AccordionItemButton, AccordionItemPanel, } from 'react-accessible-accordion';

const OT = require('@opentok/client');

class JoinLive extends Component {
	subscriberIntervalID = 0;
	sessionIntervalID = 0;

	constructor(props) {
		super(props);
		this.otSubscriber = React.createRef();
		this.state = {
			error: null,
			connection: 'Connecting',
			sessionId: '',
			token: '',
			teacherName: '',
			messageList: [],
			user: '',
			subcriberCount: 0,
			subscriberList: [],
			liveStatus: 0,
			audio: true,
			video: true
		};

		this.sessionEventHandlers = {
			sessionConnected: () => {
				this.setState({ connection: 'Connected' });
			},
			sessionDisconnected: () => {
				this.setState({ connection: 'Disconnected' });
			},
			sessionReconnected: () => {
				this.setState({ connection: 'Reconnected' });
			},
			sessionReconnecting: () => {
				this.setState({ connection: 'Reconnecting' });
			},
			streamCreated: () => {
				const requestOptions = {
					headers: getApiHeader()
				};
				const details = {
					username: this.state.user,
				};
				//Update stream status
				axios.post(apiRoute('update_opentok_subscriber'), details, requestOptions);
			},
			streamDestroyed: () => {
				this.getSessionId();
				this.setState({ liveStatus: 0, sessionId: '' });
				const requestOptions = {
					headers: getApiHeader()
				};
				//Update stream status
				axios.get(apiRoute('update_stream_status/' + this.state.sessionId + '/' + 0), requestOptions)
					.then(res => {
						console.log('Status updated success');
					});
			},
			connectionCreated: (obj) => {
				console.log('stream created');
			},

			connectionDestroyed: (obj) => {
				console.log('stream Destroyed');
			}
		};

		this.publisherEventHandlers = {
			accessDenied: () => {
				console.log('User denied access to media source');
			},
			streamCreated: () => {
				console.log('Publisher stream created');
			},
			streamDestroyed: ({ reason }) => {
				console.log(`Publisher stream destroyed because: ${reason}`);
			},
		};

		this.subscriberEventHandlers = {
			videoEnabled: () => {
				console.log('Subscriber video enabled');
			},
			videoDisabled: () => {
				console.log('Subscriber video disabled');
			},
		};
	}


	componentWillMount() {

		window.scrollTo(0, 0);

		this.props.setLoadingTrue();

		const userData = getLocalStorageAuth();
		const requestOptions = {
			headers: getApiHeader()
		};
		if (userData.userDetails) {
			const firstName = userData.userDetails.first_name;
			const lastName = userData.userDetails.last_name;
			const fullName = firstName + ' ' + lastName;
			this.setState({ user: fullName });
			const details = {
				username: fullName,
			};
			//Add user in subscriber list
			axios.post(apiRoute('update_opentok_subscriber'), details, requestOptions);
		}
		//Get session id and token
		axios.get(apiRoute('get_opentok_token'), requestOptions)
			.then(res => {
				if (res.data.status == 1) {
					this.setState({ sessionId: res.data.sessionId, token: res.data.token, teacherName: res.data.name, liveStatus: res.data.status });
				}
				this.props.clearAllAlerts();
				this.getSessionId();
			})
			.catch(error => {
				this.getSessionId();
				this.props.clearAllAlerts();
			});


	}

	onSessionError = error => {
		this.setState({ error });
	};

	onPublish = () => {
		console.log('Publish Success');
	};

	onPublishError = error => {
		this.setState({ error });
	};

	toggleVideo = () => {
		this.setState(state => ({
			publishVideo: !state.publishVideo,
		}));
	};

	getSessionId() {
		//Check new stream status
		this.sessionIntervalID = setInterval(() => {
			if (!this.state.sessionId) {
				const requestOptions = {
					headers: getApiHeader()
				};
				axios.get(apiRoute('get_opentok_token'), requestOptions)
					.then(res => {
						if (res.data.status == 1) {
							this.setState({ sessionId: res.data.sessionId, token: res.data.token, teacherName: res.data.name, liveStatus: res.data.status });
						}
					});
			}

		}, 5000)
	}
	onSubscribe = () => {
		console.log('Subscribed sucessfully');
	}

	componentDidMount() {
		window.scrollTo(0, 0);

		this.subscriberIntervalID = setInterval(() => {
			const requestOptions = {
				headers: getApiHeader()
			};
			//Get all subscriber list
			axios.get(apiRoute('get_all_stream_subscriber'), requestOptions)
				.then(res => {
					this.setState({ subscriberList: res.data.subscriers });
				});
		}, 5000)
	}

	componentWillUnmount() {
		clearInterval(this.subscriberIntervalID);
		clearInterval(this.sessionIntervalID);
		if (this.otSubscriber.current !== null) {
			const subsciberDetails = this.otSubscriber.current.getSubscriber();

			if (subsciberDetails) {

				const requestOptions = {
					headers: getApiHeader()
				};

				const details = {
					id: subsciberDetails.id,
					username: this.state.user,
				};
				//Remove user from subscriber list
				axios.post(apiRoute('remove_opentok_subscriber'), details, requestOptions);
			}
		}
	}

	setAudio = (audio) => {
		this.setState({ audio });
	}

	setVideo = (video) => {
		this.setState({ video });
	}

	render() {
		const apiKey = '46066492';
		const { sessionId, token } = this.state;
		return (
			<>
				<div className="broadcast-header">
					{this.state.liveStatus == 0 ?
						<h4 className="user-name mb-0">Welcome to Sattva Connect Live Streaming, Waiting for stream to start.</h4>
						:
						<h4 className="user-name mb-0">Welcome to Sattva Connect Live Streaming, {this.state.teacherName} is Online</h4>
					}

				</div>
				<div className="row m-0">
					<div className="col-lg-7 col-md-6 broadcast-left text-center">
						<div className="broadcast-btns"></div>
						{sessionId ? (
							<OTSession
								apiKey={apiKey}
								sessionId={sessionId}
								token={token}
								onError={this.onSessionError}
								eventHandlers={this.sessionEventHandlers}
							>
								<OTStreams>
									<OTSubscriber
										properties={{
											width: '100%', height: '100%', insertMode: 'append',
										}}
										retry={true}
										onSubscribe={this.onSubscribe}
										onError={this.onSubscribeError}
										eventHandlers={this.subscriberEventHandlers}
										ref={this.otSubscriber}
										restrictFrameRate={true}
									/>
								</OTStreams>
							</OTSession>
						) : null}
					</div>
					<div className="col-lg-2 col-md-2 broadcast-middle">
						<div className="broadcast-logo my-2">
							<img src="../images/footer_logo.png" alt="" />
						</div>
						<Accordion className="viwerList" allowZeroExpanded={true}>
							<AccordionItem>
								<AccordionItemHeading className="viwerListHeader">
									<AccordionItemButton>Online viewers <span className="new badge">{this.state.subscriberList.length}</span></AccordionItemButton>
								</AccordionItemHeading>
								<AccordionItemPanel>
									{this.state.subscriberList.map((item, index) => {
										return (
											<p className="text-success">{item}</p>
										);
									})}
								</AccordionItemPanel>
							</AccordionItem>
						</Accordion>
						<h4 className="mb-0">Online viewers <span className="new badge">{this.state.subscriberList.length}</span></h4>
						<div className="broadcastUserList">
							{this.state.subscriberList.map((item, index) => {
								return (
									<p className="text-success" key={index}>{item}</p>
								);
							})}
						</div>
					</div>
					<Chat userType="0" />
				</div>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	alert: state.alert,
	stream: state.stream,
});
export default connect(mapStateToProps, { updateChat, setLoadingTrue, clearAllAlerts })(JoinLive);