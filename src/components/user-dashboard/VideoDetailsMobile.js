import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ReactTooltip from "react-tooltip";


function VideoDetailsMobile({ item }) {

  return (
    <Link to={"/user-dashboard/video-details/" + item.id}>
      <div className="hoverable card" key={item.id}>
        <img className="img-fluid" src={item.thumbnail} />
        <div className="card-content">
          <span className="card-title" data-html={true} data-for='custom-color-no-arrow' data-tip={item.title}>{item.title}</span>
          <ReactTooltip id='custom-color-no-arrow' className='react-tooltip card-title-tooltip' delayHide={1000} textColor='#FFF' backgroundColor='#5c1b72' effect='solid'/>
          <p className="pop" dangerouslySetInnerHTML={{ __html: item.description }}></p>
        </div>
      </div>
    </Link>
  );
}

export default VideoDetailsMobile;