import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../../utils/helpers';

class RecentPost extends Component{

	constructor(props){
    	super(props);
	      this.state = {
	        posts:[],
		    } 
		  }

	componentWillMount() {
		
    window.scrollTo(0, 0);

    const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-all-posts'),requestOptions)
		.then(res =>{
			this.setState({ posts : res.data });
	    })
	}

	render(){
		return(
		<>
		{this.state.posts && this.state.posts.length !== 0 ? 
		  <section className="sec sec-blog d-none">
		        <div className="container">
		            <div className="text-center mb-4">
		                <h2>Recent Posts</h2>
		            </div>
		            <div className="row">
		            {this.state.posts.map((item,index)=>{
		         	return(
		                <div className="col-md-4" key={index}>
		                    <div className="card">
								<div className="imgvid-div">
		                       	 <img className="card-img" src={item.image}/>
								</div>
		                        <div className="card-body">
		                          <h5 className="card-title">{item.title}</h5>
		                          <p className="card-text mt-2">
		                          {item.description}
		                          </p>
		                        </div>
		                    </div>
		                </div>
		                 );
			       	})}	
		            </div>
		        </div>
		    </section>
		      : ''}
		      </>
		);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(RecentPost);