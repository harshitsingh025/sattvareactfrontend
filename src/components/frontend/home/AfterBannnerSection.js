import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { imagePath } from '../../../utils/helpers';

function AfterBannnerSection({sections, getImage}){
		
		return(
			<>
			 <section id="livein" className="sec sec-fvideo text-center">
			      <div className="container">
			        <iframe className="responsive-video" src={sections['0'].link} frameBorder="0" width="100%" height="480" allowFullScreen></iframe>
			     </div>
			     <h2>Living Enlightenment</h2>
				 <div className="container mt-5">
			        <div className="row">
			          <div className="col-md-12 text-white text-center">
			           
			            <Link to='/plans' className="btn btn-lg">Start your free trial now</Link>
			          </div>
			        </div>
			      </div>
			</section>

			<section className="sec bg-cover bg-position-left" style={getImage(imagePath('cms/'+sections['1'].image))}>
			      <div className="container">
			        <div className="row">
			          <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12"></div>
			          <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 about-text">
			               <h2 className="mb-2">{sections['1'].title}</h2>
				            <div dangerouslySetInnerHTML={{__html: sections['1'].description}}>
						 	</div>
			              <ul>
			                <li><a target="_blank" href="https://www.facebook.com/sattvaconnect/"><i className="fa fa-facebook"></i></a></li>
			                <li><a target="_blank" href="https://www.instagram.com/sattvaconnect/"><i className="fab fa-instagram"></i></a></li>
			              </ul>
			              <Link to={sections['1'].link} className="btn btn-lg">Learn More</Link>
			          </div>
			        </div>
			     </div>
   			</section>
   			 </>
			);
}

export default AfterBannnerSection;