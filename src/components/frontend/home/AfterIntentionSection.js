import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { imagePath } from '../../../utils/helpers';

function getBackgroundImage(image){
	var fullImage = imagePath('cms/'+image);
	var img = {
			backgroundImage: `url(${fullImage})`,
			backgroundRepeat: `no-repeat`,
			backgroundPosition: `center`,
		}
	return img;
}

function AfterIntentionSection({sections}){

		return(
			<>
		    <section className="sec sec-evolution" style={getBackgroundImage(sections['3'].image)}>
		      <div className="container">
		        <div className="row">
		          <div className="col-lg-6 col">
		              <h2>{sections['3'].title}</h2>
		              <h5   dangerouslySetInnerHTML={{__html: sections['3'].description}}></h5>
		              <Link to={sections['3'].link} className="btn btn-lg">Learn More</Link>
		          </div>
		        </div>
		      </div>
		    </section>
		    <section className="sec sec-clarity" style={getBackgroundImage(sections['4'].image)}>
		      <div className="container">
		        <div className="row justify-content-end">
		          <div className="col-lg-6 col">
		              <h2>{sections['4'].title}</h2>
		              <h5  dangerouslySetInnerHTML={{__html: sections['4'].description}}></h5>
		              <Link to={sections['4'].link} className="btn btn-lg">Learn More</Link>
		          </div>
		        </div>
		      </div>
		    </section>
		    <section className="sec sec-vitality" style={getBackgroundImage(sections['5'].image)}>
		      <div className="container">
		        <div className="row">
		          <div className="col-lg-6 col">
		              <h2>{sections['5'].title}</h2>
		              <h5 dangerouslySetInnerHTML={{__html: sections['5'].description}}></h5>
		              <Link to={sections['5'].link} className="btn btn-lg">Learn More</Link>
		          </div>
		        </div>
		      </div>
		    </section>
		     
		    </>
			);
	}

export default AfterIntentionSection;