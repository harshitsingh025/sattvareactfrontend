import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../../utils/helpers';

class Benefits extends Component{

	constructor(props){
    	super(props);
	      this.state = {
	        benefits:[],
		    } 
		  }

	componentWillMount() {
		
    window.scrollTo(0, 0);

    const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-all-benefits'),requestOptions)
		.then(res =>{
			this.setState({ benefits : res.data });
	    })
	}

	render(){
		return(
		<>
		 {this.state.benefits && this.state.benefits.length !== 0 ? 
		  <section className="sec sec-members text-center">
		      <div className="container">
		        <h2>Member Benefits</h2>
		        <div className="row">
		          {this.state.benefits.map((item,index)=>{
		         	return(
			          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12" key={index}>
			            <div className="members-content">
			              <img src={item.image}/>
			                <h5>{item.title}</h5>
			                <p>{item.description}</p>
			            </div>
			          </div>
		            );
			       	})}	
		        </div>
		      </div>
		    </section>
		      : ''}
		      </>
		);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(Benefits);