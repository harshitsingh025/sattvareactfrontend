import React,{Component} from 'react';
import { Link } from 'react-router-dom';

function HomeBanner({banner,getImage}){
	const banners = [
		{
			image:'slider1.c5dbe1df_1577254476.jpg',
			title:'Wisdom. Yoga. Meditation.',
			description:'Ancient Himalayan yogic teachings relevant to anyone on the evolutionary path.',
			link:'about-us',
		},
		{
			image:'slider2.223c64a7_1576833780.jpg',
			title:'Learn from the source',
			description:'Study with Himalayan Master, Anand Mehrotra.',
			link:'about-anandji',
		},
		{
			image:'slider3.9afd91cb_1576833953.jpg',
			title:'Authentic Teachings and Integrative Practices',
			description:'Gain access to incredible teachings and full-spectrum practices.',
			link:'about-sattva',
		},
		{
			image:'slider4.d2e639e6_1576834457.jpg',
			title:'Live from Rishikesh, the yoga capital of the world.',
			description:'Gain access to live satsangs, wisdom talks, guided meditations, and other yogic practices through our live streams channel.',
			link:'about-rishikesh',
		},
		{
			image:'slider5.913610b6_1576834506.jpg',
			title:'Connect with a vibrant & living community',
			description:'Join our sangha, a global community of inspired yogis.',
			link:'about-sangha',
		},
	]
	return(
		  <div className="view intro-2">
		    <div id="carousel-example-2" className="carousel slide carousel-fade" data-ride="carousel">
		      <div className="carousel-inner" role="listbox">
		      {banners.map((item,index)=>{
		      	var active;
				if (index == 0) {
				  active = 'active';
				} else {
				  active = '';
				}
				let image = require('../../../assets/images/banner/'+item.image);
		      	return(
		        <div className={'carousel-item '+ active} style={{
					backgroundImage: `url(${image})`,
					backgroundRepeat: `no-repeat`,
					backgroundPosition: `center`,
					}} key={index}>
		          <div className="carousel-caption text-center">
					  	<div className="mx-auto animated fadeInUp">
							<img src={require('../../../assets/images/sattvabanner-logo.png')} className="img-fluid" />
						</div>
		            <a><h1 className="sp-title animated fadeInUp">{item.title}</h1></a>
		            <h2 className="sp-posttitle animated-two fadeInUp">{item.description}</h2>
		            <div className="read-more-wrapper animated-four fadeInUp">
		              <Link to={item.link}className="btn waves-effect waves-light btn-lg">Learn more</Link>
		            </div>
		          </div>
		          </div>
		        );
		       	})}	
		      </div>
		      <a className="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
		        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
		        <span className="sr-only">Previous</span>
		      </a>
		      <a className="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
		        <span className="carousel-control-next-icon" aria-hidden="true"></span>
		        <span className="sr-only">Next</span>
		      </a>
		    </div>
		  </div>
			);
	}

export default HomeBanner;