import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import OwlCarousel from 'react-owl-carousel';
import { apiRoute, getApiHeader } from '../../../utils/helpers';

class Testimonials extends Component{

	constructor(props){
    	super(props);
	      this.state = {
	        testimonials:[],
		    } 
		  }

	componentWillMount() {
		
    window.scrollTo(0, 0);

    const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-all-testimonials'),requestOptions)
		.then(res =>{
			this.setState({ testimonials : res.data });
	    })
	}

	render(){
		return(
		<>
		 {this.state.testimonials && this.state.testimonials.length !== 0 ? 
		  <section className="sec sec-testimonial text-center">
		      <div className="container">
		        <h2>What The Community Is Saying</h2>
		         <OwlCarousel id="sattvatestimonial" className="owl-carousel owl-theme testimonial-block" nav={true} items={1} dots={false}>
		        {this.state.testimonials.map((item,index)=>{
		         return(
		           <div className="row align-items-center justify-content-center" key={index}>
		              <div className="col-lg-5 col-md-5 col-sm-12">
		                 <a className="modal-trigger img-fluid" data-toggle="modal" data-target="#testimonial" data-backdrop="static" data-keyboard="false" onClick={ () => this.props.changeTestimonialLink(item.videoLink)}>
		                 <img src={item.image} alt=""/>
		                 </a>
		              </div>
		              <div className="col-lg-5 col-md-5 col-sm-12 text-left">
		                 <p>{item.quote}</p>
		                 <h4>{item.authorName}, {item.authorAddress}</h4>
		                 <span></span>
		              </div>
		           </div>
		            );
			       	})}	
		        </OwlCarousel>
		      </div>
		    </section>
		      : ''}
		      </>
		);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(Testimonials);