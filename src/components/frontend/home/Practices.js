import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../../utils/helpers';

class Practices extends Component{

	constructor(props){
    	super(props);
	      this.state = {
	        practices:[],
		    } 
		  }

	componentWillMount() {
		
    window.scrollTo(0, 0);

    const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('cms-all-practices'),requestOptions)
		.then(res =>{
			this.setState({ practices : res.data });
	    })
	}

	render(){
		return(
			<>
		 {this.state.practices && this.state.practices.length !== 0 ? 
			<section className="sec sec-practice">
		      <div className="container text-center text-white">
		        <h2>Practices</h2>
		        <div className="row">
		          {this.state.practices.map((item,index)=>{
		          return(
			          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12" key={index}>
			            <figure className="par_wrap">
			              <div className="par_title">{item.title}</div>
			              <figcaption className="par_title_desc hath">{item.description}</figcaption>
			            </figure>
			          </div>
		            );
			       	})}	
		        </div>
		      </div>
			</section>
			  : ''}
		      </>
			);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(Practices);