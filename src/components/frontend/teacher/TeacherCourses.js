import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { courseImagePath, getLocalStorageAuth} from '../../../utils/helpers';


function TeacherCourses({course}){
  const auth = getLocalStorageAuth();
  let purchasedCoursesId = false;
  let isPurchased = false;
  if(auth){
     purchasedCoursesId = auth.purchased_courses;
     isPurchased = purchasedCoursesId.includes(JSON.stringify(course.id));
  }
	return(
		<div className="videos-intabs d-flex">
          <div className="card-course-img">
            <img src={courseImagePath(course.image)} className="img-fluid"/>
          </div>
          <div className="card-course-content">
            <div className="course-heading"><h6>{course.title}</h6></div>
            <div className="course-rating">
              <span className="course-price">${course.price}<small>USD</small></span>
              {/*<i className="fas fa-star"></i> <i className="fas fa-star"></i> <i className="fas fa-star"></i> <i className="fas fa-star"></i> <i className="far fa-star"></i>*/}
              <span className="course-duration"><img src="../../../images/calendar.svg" /> { course.duration}   {course.duration_type}</span>
            </div>
            <p>{course.description}</p>
            <strong>Course Benefits</strong>
            <ul className="course-benfits">
              {course.benefits && course.benefits.map((benefit,index) => {
                  return(
                      <li key={index}>{benefit}</li>
                      );
              })}
            </ul>
            <strong>Course Offerings</strong>
            <ul className="course-offer">
             {course.offering && course.offering.map((offer,index) => {
                  return(
                      <li key={index}><i className={offer.class}></i> {offer.name}</li>
                      );
              })}
            </ul>
            {auth == false &&
               <div className="card-course-btns">
                <Link to={window.PUBLIC_URL+'/buy_course/'+btoa(course.id)} className="btn btn-sm" id={course.id}>Buy Now</Link>
                <Link to={window.PUBLIC_URL+'/gift_course/'+btoa(course.id)} className="btn btn-sm">Gift To Friend </Link>
                <Link to={window.PUBLIC_URL+'/contact-us'} className="btn btn-sm">Contact Us</Link>
              </div>
              }
              {auth !== false &&
               <div className="card-course-btns">
                {isPurchased == true ? 
                <Link to={window.PUBLIC_URL+'/user/course-details/'+btoa(course.id)} className="btn btn-sm" id={course.id}>Start Course</Link>
                : 
                <Link to={window.PUBLIC_URL+'/user/buy_course/'+btoa(course.id)} className="btn btn-sm" id={course.id}>Buy Now</Link>
               }
                
                <Link to={window.PUBLIC_URL+'/user/gift_course/'+btoa(course.id)} className="btn btn-sm">Gift To Friend </Link>
                <Link to={window.PUBLIC_URL+'/user/contact-us'} className="btn btn-sm">Contact Us</Link>
              </div>
              }
          </div>
        </div>
		);
}

export default TeacherCourses;