import React,{Component} from 'react';
import { Link } from 'react-router-dom';

function PlanDetails({item}){

	return(
        <div className="col-md-6">            
        <div className="plans-container">
        <div className="card center-align">
            <div className="">
             <div className="card-image grey darken-4 ">{item.name}</div>
             </div>
            <div className="osm-item-description clearfix">
            <div className="osm-item-description-text text-center">
              <p>{item.description}</p>
            </div>
            <div className="span12">
                <table className="table table-bordered border-0">

                    <tr className="osm-plan-property">
                        <td className="osm-plan-property-label">Price: </td>
                     <td className="osm-plan-property-value">${item.price}</td>
                    </tr>
                    {/* <tr className="osm-plan-property">
                     <td className="freeTrailText">14 days free trial, Cancel anytime</td>
                    </tr> */}
                </table>
                <p className="purpleText mb-3  text-center"><i>14 days free trial, Cancel anytime</i></p>
            </div>
            <div className="card-action text-center">
                <Link to={'user-registration/'+item.id} className="btn btn-lg">Free Trial</Link>
            </div>
            </div>
        </div>
        </div>
    </div>
		);
}

export default PlanDetails;