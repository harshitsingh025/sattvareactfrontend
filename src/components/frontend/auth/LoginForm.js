import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import cookie from 'react-cookies';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import SimpleReactValidator from 'simple-react-validator';
import * as authAction from '../../../actions/authAction';
import { clearAllAlerts } from '../../../actions/userAction';

class UserLogin extends Component {

	constructor(props) {
		super(props);
		this.props.clearAllAlerts();
		this.validator = new SimpleReactValidator();
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.state = {
			username: '',
			password: '',
			labelClass:'',
		}
	}
	componentDidMount() {
		const cookieUsername = cookie.load('username');
		const cookiePassword = cookie.load('password');
		const auth = cookie.load('auth');
		console.log(auth);
		if(cookieUsername !== undefined){
			this.setState({ username: cookieUsername });
			this.setState({ labelClass: 'active' });
		}
		if(cookiePassword !== undefined){
			this.setState({ password: cookiePassword });
			this.setState({ labelClass: 'active' });
		}
		
		window.scrollTo(0, 0);
	}

	onChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	onSubmit(e) {
		e.preventDefault();
		if (!this.validator.allValid()) {
			this.validator.showMessages();
			this.forceUpdate();
			return false;
		}
		this.props.subscriberLogin(this.state.username, this.state.password, this.props.history)

	}

	render() {
		const { alert } = this.props;
		return (
			<>
			<ToastsContainer store={ToastsStore} position={ToastsContainerPosition.TOP_RIGHT} />
				
				{alert && alert.isloading &&
					<div className="preloader-background">
						<div class="big sattva_loader active">
							<img src={require('../../../assets/images/loader.png')} />
						</div>
					</div>
				}
                    <div className="row justify-content-center">
                        <div className="col-md-8 col-sm-12">
                            {alert.message && alert.type === 'error' &&
                                <div className="alert alert-danger" role="alert">
                                    {alert.message}
                                </div>}
                            {alert.message && alert.type === 'success' &&
                                <div className="alert alert-success" role="alert">
                                    {alert.message}
                                </div>}
                            <div className="card subscription-card customer-support">
                                <form autocomplete="off" className="form form-horizontal" onSubmit={this.onSubmit}>
                                    <h4>Member Login</h4>
                                    <div className="">
                                        <div className="input-field">
                                            <label id="username-lbl" htmlFor="username" className={this.state.labelClass} >Username or Email<span className="required">&#160;*</span></label>
                                            <input type="text" name="username" id="username" value={this.state.username} onChange={this.onChange} />
                                            {this.validator.message('username', this.state.username, 'required')}
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="input-field">
                                            <label id="password-lbl" htmlFor="password" className={this.state.labelClass}>Password<span className="required">&#160;*</span></label>
                                            <input type="password" name="password" id="password" aria-required="true" value={this.state.password} onChange={this.onChange} />
                                            {this.validator.message('password', this.state.password, 'required')}
                                        </div>
                                    </div>
                                    <div className="login-links">
                                        <div className="input-field">
                                            <Link to="forgot-password">Forgot your password?</Link>
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="input-field s12">
                                            <button type="submit" className="btn btn-lg">LOGIN</button>
                                        </div>
                                    </div>
                                </form>
								
                            </div>
							<div class="card subscription-card customer-support mt-20">
								<h4>Not A Member?</h4>
								<div class="sattva_login_inner_note">If you don't have an account please click on button give below.</div>
								<div class="">
									<div class="input-field">
									<ul>
										<li><Link class="btn btn-lg" to="/plans">BECOME A MEMBER</Link></li>
									</ul>
									</div>
								</div>
								</div> 
                        </div>
                </div>
			</>
		);
	}
}

const mapStateToProps = (state) => ({
	alert: state.alert,
});
const mapActionsToProps = ({
	subscriberLogin: authAction.subscriberLogin,
	clearAllAlerts: clearAllAlerts,
})
export default connect(mapStateToProps, mapActionsToProps)(UserLogin);