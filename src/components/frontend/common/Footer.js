import React,{Component} from 'react';
import { Link } from 'react-router-dom';
import { getLocalStorageAuth } from '../../../utils/helpers';
import Recaptcha from 'react-google-invisible-recaptcha';

class Footer extends Component{
		
	constructor(props){
    super(props);
  	const userData = getLocalStorageAuth();
  	if(!userData){
  		 this.state = {
		      auth : false,
		  	}
  		}else{	
  		this.state = {
		      auth : true,
		  	}	
  		}
 	 }

	render(){
		//if(!this.state.auth){
			return(
			<>
			 <Recaptcha
			ref={ ref => this.recaptcha = ref }
			sitekey='6LcXXb0ZAAAAAPKkrgEa8LPjOazq4InauFR3azbD'
			onResolved={ () => console.log( 'Human detected.' ) } />
			 <footer>
			    <div className="container">
			      <div className="mailchaimp-block center-align color-white">
			        <h4>Stay Connected</h4>
			        <p>Receive weekly inspirations, news, and updates on upcoming Live Streams, Sattva events, and new classes and courses added.</p>
			       <div id="mc_embed_signup">
			          <form action="https://sattvaconnect.us18.list-manage.com/subscribe/post?u=fdacecbe148d534ba550da181&amp;id=f39d089b44" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
			            <div id="mc_embed_signup_scroll" className="input-field row">
			              <div className="mc-field-group input-field col col-md-9 col-12">
			                <label htmlFor="mce-EMAIL">Email Address </label>
			                <input type="email" name="EMAIL" className="email text-white" id="mce-EMAIL"/>
			              </div>
			              <div className="col col-md-3 col-12 input-field">
			              <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" className=" btn btn-lg"/>
			              </div>
			             </div>
			          </form>
			       </div>
			      </div>
			      <div className="footer-info-block text-white text-lg-left text-center">
			        <div className="row">
			          <div className="col-lg-4 col-md-12 col-12">
			              <div className="row">
			                 <div className="col-lg-4 col-md-12 col-12">
			                    <img src={require('../../../assets/images/email.png')} className="img-fluid" alt=""/>
			                 </div>
			                 <div className="col-lg-8 col-md-12 col-12">
			                    <h5>Email</h5>
			                    <p>info@sattvaconnect.com</p>
			                 </div>
			              </div>
			           </div>
			           <div className="col-lg-4 col-md-12 col-sm-12 text-center">
			              <h5>Follow Us</h5>
			              <ul className="socillink-block">
			                 <li><a target="_blank" href="https://www.facebook.com/sattvaconnect/"><i className="fa fa-facebook" aria-hidden="true"></i></a></li>
			                 <li><a target="_blank" href="https://twitter.com/SattvaConnect"><i className="fab fa-twitter" aria-hidden="true"></i></a></li>
			                 <li><a target="_blank" href="https://www.instagram.com/sattvaconnect/"><i className="fab fa-instagram" aria-hidden="true"></i></a></li>
			                 <li><a target="_blank" href="https://www.pinterest.com/sattvaconnect/"><i className="fab fa-pinterest-p"></i></a></li>
			              </ul>
			           </div>
			           <div className="col-lg-4 col-md-12 col-12">
			              <div className="row">
			                 <div className="col-lg-4 col-md-12 col-12">
			                    <img src={require('../../../assets/images/visit.png')} className="img-fluid" alt=""/>
			                 </div>
			                 <div className="col-lg-8 col-md-12 col-12">
			                    <h5>Visit Us</h5>
			                    <p>Sattva Retreat, Mohan Chatti, Rishikesh, Himalayas - India.  </p>
			                 </div>
			              </div>
			           </div>
			        </div>
			      </div>
			      <div className="row">
			      <div className="col-lg-6 col-md-12 col-sm-12">
			        <div className="text-lg-left text-center">
			          <h4>Links</h4>
			          <ul className="nav foot-links">
			            <li>
			            <Link to="/about-us">About Us</Link>
			            </li>
			            <li><Link to="/contact-us">Contact</Link></li>
			            <li><Link to="/privacy-policy">Privacy Policy</Link></li>
			            <li><Link to="/terms-of-services">Terms of Service</Link></li>
			            <li><Link to="/customer-support">Support</Link></li>
			            <li><Link to="/faq">FAQ</Link></li>
			            {/* <li><Link to="/teachers">Teachers</Link></li>
			            <li><Link to="/team">Sattva Connect Team</Link></li> */}
			            <li><a href="https://courses.sattvaconnect.com" target="_blank"> Courses</a></li>
			          </ul>
			        </div>
			      </div>
			      <div className="col-md-6 col-sm-12"></div>
			      </div>
			    </div>
			  </footer>
			  	<section className="bottom-footer">
				    <div className="container">
				      <div className="daily-block text-center">
				        <h5></h5>
				        <p>Your purpose here is to evolve, to transform, to experience your radical aliveness, to awaken to your true nature. You are the path. The path is you. The time is now </p>
				        <span></span>
				        <img className="logo-img" src={require('../../../assets/images/footer_logo.png')} alt="Footerlog"/>
				        <p className="footer-copyright">Copyright © 2020 Sattva Connect, LLC</p>
				      </div>
				    </div>
				 </section>
				 </>
			);
		// }else{
		// 	return <div></div>
		// }
	}
}

export default Footer;