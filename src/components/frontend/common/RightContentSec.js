import React,{Component} from 'react';

function RightContentSec({children, sectionClass}){
		return(
			 <section className={sectionClass}>
		      <div className="container">
		        <div className="row">
		          <div className="col-xl-6 col-lg-6 col-12"></div>
		          <div className="col-xl-6 col-lg-6 col-12 about-text">
		              {children}
		          </div>
		        </div>
		     </div>
		    </section>
			);
	}

export default RightContentSec;