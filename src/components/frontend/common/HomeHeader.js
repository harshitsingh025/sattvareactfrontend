import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { getLocalStorageAuth } from '../../../utils/helpers';

class Header extends Component {

	constructor(props) {
		super(props);
		this.onDeleteClick = this.onDeleteClick.bind(this);
		const userData = getLocalStorageAuth();

		if (!userData) {
			this.state = {
				redirect: false,
				auth: false,
				first_name: '',
				last_name: '',
			}
		} else {
			const firstName = userData.userDetails.first_name;
			const lastName = userData.userDetails.last_name;
			this.state = {
				redirect: true,
				auth: userData,
				first_name: firstName,
				last_name: lastName,
			}
		}
	}

	onDeleteClick() {
		localStorage.removeItem('auth');
		this.setState({
			redirect: true,
		});
	}

	render() {
		const { alert } = this.props;
		if (this.state.redirect) {
			return (
				<Redirect to={window.PUBLIC_URL + '/user-dashboard/me'} />
			);
		}
		if (this.state.auth.userDetails) {
			return (
				<>
					{alert && alert.isloading &&
						<div className="preloader-background">
							<div className="big sattva_loader active">
								<img src={require('../../../assets/images/loader.png')} />
							</div>
						</div>
					}
					 <header className="sidebar-head user-header">
					 <img className="user-logo img-fluid" src={require('../../../assets/images/logo-sattva.png')} alt="Sattva Connect" />
                    <button type="button" id="sidebarCollapse" className="btn btn-toggle">
                    <i className="fas fa-bars"></i>
                    </button>
                    <nav id="sidebar" className="active">
                    <ul className="list-unstyled components">
                        <li>
                        <Link to="/user-dashboard/explore">
                            <i className="fas fa-eye"></i><span>Explore</span>
                        </Link>
                        </li>
                        <li>
                        <Link to="/user-dashboard/me">
                            <i className="fas fa-user"></i><span>Me</span>
                        </Link>
                        </li>
                        <li>
                        <Link to="/user-dashboard/search">
                            <i className="fas fa-search"></i><span>Search</span>
                        </Link>
                        </li>
                        <li>
                        <Link to="/user-dashboard/live-stream">
                            <i className="fas fa-chalkboard-teacher"></i><span>Live Stream</span>
                        </Link>
                        </li>
						<li>
                        <Link to="/user-dashboard/support">
                            <i className="fas fa-life-ring"></i><span>Support</span>
                        </Link>
                        </li>
                        <li>
                        <Link to="/user-dashboard/settings">
                            <i className="fas fa-cog"></i><span>Settings</span>
                        </Link>
                        </li>
                        <li>
                        <a className=""  onClick={this.onDeleteClick}>
                            <i className="fas fa-sign-out-alt"></i><input type="button" className="btn btn-sm" name="Submit" value="Logout"/>
                        </a>
                        </li>
                    </ul>
                    </nav>
                </header>
				</>
			);
		} else {
			return (
				<>

					{alert && alert.isloading &&
						<div className="preloader-background">
							<div className="big sattva_loader active">
								<img src={require('../../../assets/images/loader.png')} />
							</div>
						</div>
					}
					<header className="home-header">
						<nav className="navbar fixed-top navbar-expand-lg scrolling-navbar">
							<div className="navbar-brand">
								<div className="logo-image">
									<Link to="/" className="lg-w">
										<img src={require('../../../assets/images/logo-sattva-white.png')} alt="" />
									</Link>
									<Link to="/" className="lg-c">
										<img className="logo-img" src={require('../../../assets/images/logo-sattva.png')} alt="Sattva Connect" />
									</Link>
								</div>
							</div>
							<button className="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
								<span className="navbar-toggler-icon"></span>
							</button>
							<div className="collapse navbar-collapse" id="navbar">
								<ul className="navbar-nav ml-auto">
									<li className="nav-item">
										<Link to="/about-us" className="nav-link">
											About Us
				                    </Link>
									</li>
									<li className="nav-item">
										<Link to="/upcoming-stream" className="nav-link">
										Live Stream
				                    </Link>
									</li>
										<li className="nav-item">
										<a href="https://www.sattvaconnect.com/roadtodharma" target="blank" className="nav-link">
											Courses
				                    </a>
									</li>
									<li className="nav-item">
										<Link to="/login" className="nav-link">Login</Link>
									</li>
									<li className="nav-item">
										<Link to="/plans" className="nav-link btn signup-btn">Free Trial</Link>
									</li>
								</ul>
							</div>
						</nav>
					</header>
				</>
			);
		}

	}
}

const mapStateToProps = (state) => ({
	alert: state.alert,
});

export default connect(mapStateToProps, null)(Header);