import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../../utils/helpers';

class PhoneCodeOptions extends Component{

	constructor(props){
    super(props);
      this.state = {
        countries : [],
    } 
  }

	componentWillMount() {
    const requestOptions = {
			headers : getApiHeader()
		};
		axios.get(apiRoute('get-all-countries'),requestOptions)
		.then(res =>{
			this.setState({ countries : res.data.countries });
	    })
	}

	render(){
		return(
			<>
			{this.state.countries.map((item,index)=>{
				return(
					<option key={index} value={item.phonecode}>{item.phonecode}</option>
				);
			})}	
			</>
			);
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(PhoneCodeOptions);