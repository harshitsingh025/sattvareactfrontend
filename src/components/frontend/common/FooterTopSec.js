import React,{Component} from 'react';
import { Link } from 'react-router-dom';

function FooterTopSec(){
		return(
			<>
			 <section className="sec sec-join">
			      <div className="container">
			        <div className="row">
			          <div className="col-md-12 join-content text-white">
			            <h2>Join Our Community</h2>
			            <Link to='/plans' className="btn btn-lg">Start your free trial now</Link>
			          </div>
			        </div>
			      </div>
			    </section>  
			</>
			);
	}

export default FooterTopSec;