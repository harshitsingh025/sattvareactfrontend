import React,{Component} from 'react';
import { Link } from 'react-router-dom';

function Banner(props){
		var banner = {
			backgroundImage: `url(${props.banner.image})`
		}
		return(
			<>
			<div className="view intro-2">
			    <section className="inner-banner" style={banner}>
			      <div className="container text-center text-white">
			      {props.banner.title ? 
			      	<h1>{props.banner.title}</h1> : ''
			      }
			         {props.banner.description ? 
			         <h2 className="sp-posttitle animated-two fadeInUp sp-animation-2">{props.banner.description}</h2> : ''
			     }
			      {props.banner.link ? 
		            <div className="read-more-wrapper animated-four fadeInUp">
		              <a href={props.banner.link} className="btn waves-effect waves-light btn-lg">Learn more</a>
		            </div> : ''
		        }
			      </div>
			    </section>
		 	</div>
			</>
			);
	}

export default Banner;