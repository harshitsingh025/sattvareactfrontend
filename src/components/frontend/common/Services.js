import React,{Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import { apiRoute, getApiHeader } from '../../../utils/helpers';

class Services extends Component{

	constructor(props){
    	super(props);
	      this.state = {
	        services:[],
		    } 
		  }

	componentWillMount() {
		
	    window.scrollTo(0, 0);

	    const requestOptions = {
				headers : getApiHeader()
			};
			axios.get(apiRoute('cms-all-services'),requestOptions)
			.then(res =>{
				if(this.props.page == 1){
					this.setState({services: res.data.filter(item => item.on_home !== '0')});
				}else{
					this.setState({ services : res.data.filter(item => item.on_home !== '2')});
				}
		    })
		}
		 validateUrl = (url) => {
			var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
			if (pattern.test(url)) {
				return true;
			} 
				return false;
			}

	render(){

		if(this.props.page == 1){
			return(
				<>
			 {this.state.services && this.state.services.length !== 0 ? 
				<section className="sec sec-three-block">
				  <div className="container text-center text-white">
					<div className="row">
						{this.state.services.map((item,index)=>{
							return(
								<>
							{item.on_home !== 0 ? 
							 <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4" key={index}>
							 {this.validateUrl(item.link) ? 
							 <>
							 <a href={item.link} target='_blank' className="three-items" style={ this.props.getImage(item.image) }>
							 <h4>{item.title}</h4>
								<p>{item.description}</p>
							 </a>
							 </>:
							 <>
							<a href={item.link} className="three-items" style={ this.props.getImage(item.image) }>
							 <h4>{item.title}</h4>
							   <p>{item.description}</p>
							</a>
							</>
							 }
						  </div>
							: ''}
							 </>
							);
						})}	
						   </div>
					  </div>
					</section>
					 : ''}
					 </>
				);
		}else{
			return(
				<>
			 {this.state.services && this.state.services.length !== 0 ? 
				<section className="sec sec-about-block">
				  <div className="container text-center text-white">
					<div className="row">
						{this.state.services.map((item,index)=>{
							return(
							 <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4" key={index}>
								<div className="three-items" style={ this.props.getImage(item.image) }>
								   <h4>{item.title}</h4>
								   <p>{item.description}</p>
								   {item.link ? <a className="waves-effect btn" href={item.link}>Learn More</a> : ''}
								   
								</div>
							 </div>
							);
						})}	
						   </div>
					  </div>
					</section>
					 : ''}
					 </>
				);
		}
		
	}
}

const mapStateToProps = (state) =>({
  alert: state.alert,
});
export default connect(mapStateToProps, {})(Services);