// module.exports = {
// 	BASE_URL: 'https://www.sattvaconnect.com/backendportal/',
// 	BASE_URL_USER: 'https://www.sattvaconnect.com/backendportal/user/',
//  	WEBSITE_URL: 'https://www.sattvaconnect.com/backendportal/api/',
// 	IMAGE_PATH: 'https://www.sattvaconnect.com/backendportal/public/images/',
// 	TEACHER_IMAGE_PATH: 'https://www.sattvaconnect.com/backendportal/public/images/teachers/',
// 	COURSE_IMAGE_PATH: 'https://www.sattvaconnect.com/backendportal/public/images/courses/',
// 	TEACHER_IMAGE_DETAILS_PATH: 'https://www.sattvaconnect.com/backendportal/public/admins/media/teachers/',
// 	TEACHER_VIDEO_DETAILS_PATH: 'https://www.sattvaconnect.com/backendportal/public/admins/images/teachers/',
// 	USER_PROFILE_PATH: 'https://www.sattvaconnect.com/backendportal/public/images/profile/',
// 	};

	module.exports = {
		BASE_URL: 'https://sattvastaging.website/',
		BASE_URL_USER: 'https://sattvastaging.website/user/',
		 WEBSITE_URL: 'https://sattvastaging.website/backendportal/api/',
		IMAGE_PATH: 'https://sattvastaging.website/backendportal/public/images/',
		TEACHER_IMAGE_PATH: 'https://sattvastaging.website/backendportal/public/images/teachers/',
		COURSE_IMAGE_PATH: 'https://sattvastaging.website/backendportal/public/images/courses/',
		TEACHER_IMAGE_DETAILS_PATH: 'https://sattvastaging.website/backendportal/public/admins/media/teachers/',
		TEACHER_VIDEO_DETAILS_PATH: 'https://sattvastaging.website/backendportal/public/admins/images/teachers/',
		 USER_PROFILE_PATH: 'https://sattvastaging.website/backendportal/public/images/profile/',
		};
