import React, { Suspense, lazy } from 'react';
import {Route , Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import initialState from './reducers/initialState'; 
import configureStore, { history }  from './store';
import PageNotFound from './pages/frontend/PageNotFound';
import Index from './pages/frontend/Index';

import UserRegistration from './pages/frontend/UserRegistration';
import Plans from './pages/frontend/Plans';
import Login from './pages/frontend/Login';
import ForgotPassword from './pages/frontend/ForgotPassword';
import ChangePassword from './pages/frontend/ChangePassword';
import UserRegistrationSuccess from './pages/frontend/UserRegistrationSuccess';
import AudioRecording from './pages/frontend/AudioRecording';

import LiveStreamMain from './pages/user/LiveStream';

import Setting from './pages/user/Setting';
import Search from './pages/user/Search';
import VideoDetails from './pages/user/VideoDetails';
import Me from './pages/user/Me';
import MyClasses from './pages/user/MyClasses';
import MyFavorites from './pages/user/MyFavorites';
import RecentWatched from './pages/user/RecentWatched';
import Recommended from './pages/user/Recommended';
import NewVideos from './pages/user/NewVideos';
import Explore from './pages/user/Explore';

import AsanaLab from './pages/user/AsanaLab';
import PranayamaLab from './pages/user/PranayamaLab';
import KriyaLab from './pages/user/KriyaLab';
import MantraLab from './pages/user/MantraLab';
import FrontendContactus from './pages/frontend/Contactus';

import LiveStream from './pages/user/broadcost/LiveStream';
import JoinStream from './pages/user/broadcost/JoinStream';

import UserFaq from './pages/user/Faq';
import UserCustomerSupport from './pages/user/CustomerSupport';
import UserSupportSuccess from './pages/user/SupportSuccess';
import UserUpcomingStream from './pages/user/UpcomingStream';

const Team = lazy(() => import('./pages/frontend/Team'));
const TeacherDetails = lazy(() => import('./pages/frontend/TeacherDetails'));
const Teachers = lazy(() => import('./pages/frontend/Teachers'));
const Aboutus = lazy(() => import('./pages/frontend/Aboutus'));
const Vitality = lazy(() => import('./pages/frontend/Vitality'));
const Evolution = lazy(() => import('./pages/frontend/Evolution'));
const AboutClasses = lazy(() => import('./pages/frontend/AboutClasses'));
const AboutAnandji = lazy(() => import('./pages/frontend/AboutAnandji'));
const AboutSattva = lazy(() => import('./pages/frontend/AboutSattva'));
const AboutRishikesh = lazy(() => import('./pages/frontend/AboutRishikesh'));
const UpcomingStream = lazy(() => import('./pages/frontend/UpcomingStream'));
const AboutSangha = lazy(() => import('./pages/frontend/AboutSangha'));
const Clarity = lazy(() => import('./pages/frontend/Clarity'));
const Forum = lazy(() => import('./pages/frontend/Forum'));
const TermsOfServices = lazy(() => import('./pages/frontend/TermsOfServices'));
const PrivacyPolicy = lazy(() => import('./pages/frontend/PrivacyPolicy'));
const Faq = lazy(() => import('./pages/frontend/Faq'));
const CustomerSupport = lazy(() => import('./pages/frontend/CustomerSupport'));
const SupportSuccess = lazy(() => import('./pages/frontend/SupportSuccess'));

function App() {

  const store = configureStore(initialState);

  return (
    <>
    <Provider store={store}>
      <ConnectedRouter  history={history}>
      <Suspense fallback={<div className="preloader-background">
							<div className="big sattva_loader active">
								<img src={require('./assets/images/loader.png')} />
							</div>
						</div>}>
          <Switch>
             <Route exact path="/" component={Index}/> 
             <Route exact path="/team" component={Team}/>
            <Route exact path="/teacher-details/:teacherId" component={TeacherDetails}/>
            <Route exact path="/teachers" component={Teachers}/> 
             <Route exact path="/about-us" component={Aboutus}/>
            <Route exact path="/vitality" component={Vitality}/>
            <Route exact path="/evolution" component={Evolution}/>
            <Route exact path="/about-classes" component={AboutClasses}/>
            <Route exact path="/about-rishikesh" component={AboutRishikesh}/>
            <Route exact path="/about-anandji" component={AboutAnandji}/>
            <Route exact path="/about-sattva" component={AboutSattva}/>
            <Route exact path="/upcoming-stream" component={UpcomingStream}/>
            <Route exact path="/about-sangha" component={AboutSangha}/>
            <Route exact path="/clarity" component={Clarity}/>
            <Route exact path="/contact-us" component={FrontendContactus}/>
            <Route exact path="/forum" component={Forum}/>
            <Route exact path="/terms-of-services" component={TermsOfServices}/>
            <Route exact path="/privacy-policy" component={PrivacyPolicy}/>
            <Route exact path="/faq" component={Faq}/>
            <Route exact path="/customer-support" component={CustomerSupport}/> 
            <Route exact path="/support-success" component={SupportSuccess}/> 
            <Route exact path="/plans" component={Plans}/>
            <Route exact path="/audio-recording" component={AudioRecording}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/forgot-password" component={ForgotPassword}/>

            <Route exact path="/user-registration/:planId" component={UserRegistration}/>
            <Route exact path="/user-registration-success" component={UserRegistrationSuccess}/>
            <Route exact path="/change-password/:id" component={ChangePassword}/>
            <Route exact path="/user-dashboard/go-live" component={LiveStream}/>
            <Route exact path="/user-dashboard/join-live" component={JoinStream}/>
            <Route exact path="/user-dashboard/live-stream" component={LiveStreamMain}/>
            <Route exact path="/user-dashboard/support" component={UserCustomerSupport}/>
            <Route exact path="/user-dashboard/support-success" component={UserSupportSuccess}/>
            <Route exact path="/user-dashboard/faq" component={UserFaq}/>
            <Route exact path="/user-dashboard/upcoming-stream" component={UserUpcomingStream}/>

            <Route exact path="/user-dashboard/settings" component={Setting}/>
            <Route exact path="/user-dashboard/search" component={Search}/>
            <Route exact path="/user-dashboard/video-details/:id" component={VideoDetails}/>
            <Route exact path="/user-dashboard/me" component={Me}/>
            <Route exact path="/user-dashboard/my-classes" component={MyClasses}/>
            <Route exact path="/user-dashboard/my-favorites" component={MyFavorites}/>
            <Route exact path="/user-dashboard/recent" component={RecentWatched}/>
            <Route exact path="/user-dashboard/recommended" component={Recommended}/>
            <Route exact path="/user-dashboard/new-videos" component={NewVideos}/>
            <Route exact path="/user-dashboard/explore" component={Explore}/>
            <Route exact path="/user-dashboard/asana-lab" component={AsanaLab}/>
            <Route exact path="/user-dashboard/prayanama-lab" component={PranayamaLab}/>
            <Route exact path="/user-dashboard/kriya-lab" component={KriyaLab}/>
            <Route exact path="/user-dashboard/mantra-lab" component={MantraLab}/>
            <Route exact path="/user-dashboard/join-live-stream" component={LiveStream}/>
            <Route component={PageNotFound}/>
          </Switch>
          </Suspense>
        </ConnectedRouter>
    </Provider>
    </>
  );
}

export default App;
