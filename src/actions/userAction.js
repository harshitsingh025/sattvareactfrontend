import * as types from './types';
import UserServices from '../services/userServices';

export function sendContactMail(data, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.sendContactMail(data)
            .then(res => {
                dispatch({ type: types.ALERT_SUCCESS, message: res.data.message, isloading: false });
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function updateUserDetails(data, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.updateUserDetails(data)
            .then(res => {
                dispatch({ type: types.USER_UPDATE_SUCCESS, payload: res.data });
                dispatch({ type: types.ALERT_SUCCESS, message: 'Deatils has been updated successfully', isloading: false });
                
                let auth = JSON.parse(localStorage.getItem('auth'));
                auth.userDetails = res.data;
                localStorage.setItem('auth', JSON.stringify(auth));
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function changeUserPassword(data, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.changeUserPassword(data)
            .then(res => {
                dispatch({ type: types.USER_UPDATE_SUCCESS, payload: res.data });
                dispatch({ type: types.ALERT_SUCCESS, message: 'Password has been changed successfully', isloading: false });
             
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function clearAllAlerts() {
    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR });
    }
}

export function setLoadingTrue() {
    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
    }
}

export function deleteUserAccount(id, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.deleteUserAccount(id)
            .then(res => {
                dispatch({ type: types.ALERT_SUCCESS, message: 'User has been deleted successfully', isloading: false });
                localStorage.removeItem('auth');
                history.push('/userlogin');
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function buyNewCourse(data, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.buyNewCourse(data)
            .then(res => {
                dispatch({ type: types.ALERT_SUCCESS, message: 'Course has been purchased successfully', isloading: false });
                history.push('/user/dashboard');
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function sendForgotPasswordMail(data) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.sendForgotPasswordMail(data)
            .then(res => {
                dispatch({ type: types.ALERT_SUCCESS, message: 'Please check your email to reset your password.', isloading: false });
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function sendCustomerSupportMail(data) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        UserServices.sendCustomerSupportMail(data)
            .then(res => {
                dispatch({ type: types.ALERT_SUCCESS, message: res.data.message, isloading: false });
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}
