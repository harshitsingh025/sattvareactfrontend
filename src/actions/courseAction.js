import * as types from './types';
import CourseServices from '../services/courseServices';

export const fetchCourse = (from, courses) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.fetchCourse(from)
        .then(res => {
            let allCourses = [...courses, ...res.data.courses];
            dispatch({ type: types.FETCH_COURSE, cursor: res.data.cursor, hasMore: res.data.hasMore, courses: allCourses });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchCourseDetail = (id, history) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.fetchCourseDetail(id)
        .then(res => {
            dispatch({ type: types.FETCH_COURSE_DETAILS, payload: res.data });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
                history.push('/');
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                history.push('/');
            }
        });

}

export const buyCourseWithRegistration = (data, history) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.buyCourseWithRegistration(data)
        .then(res => {
            CourseServices.sendPurchaseCourseMail(res.data.userDetails,res.data.course);
            dispatch({ type: types.ALERT_SUCCESS, message: 'Course purchased successfully' });
            
            let auth = JSON.parse(localStorage.getItem('auth'));
            auth.userDetails = res.data.userDetails;
            localStorage.setItem('auth', JSON.stringify(auth));
            history.push('/payment_success');
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });
}

export const storeUserData = (data) => dispatch => {
    dispatch({ type: types.STORE_USER_DETAILS, payload: data });
}

export const getClintId = () => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.getClintId()
        .then(res => {
            dispatch({ type: types.FETCH_BRAINTREE_CLINT_ID, payload: res.data });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });
}

export const fetcUserCourses = (userId, from, courses) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.fetchUserCourses(userId, from)
        .then(res => {
            let allCourses = [...courses, ...res.data.courses];
            dispatch({ type: types.FETCH_USER_COURSE, courses: allCourses, cursor: res.data.cursor, hasMore: res.data.hasMore });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchUserAvailableCourses = (userId, from, courses) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.fetchUserAvailableCourses(userId, from)
        .then(res => {
            let allCourses = [...courses, ...res.data.courses];
            dispatch({ type: types.FETCH_USER_AVAILABLE_COURSE, courses: allCourses, cursor: res.data.cursor, hasMore: res.data.hasMore });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetcCourseAllDetails = (userId) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.fetchCourseAllDetails(userId)
        .then(res => {
            dispatch({ type: types.FETCH_COURSE_ALL_DETAILS, payload: res.data });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchRelatedCourses = (userId, courseId) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    CourseServices.fetchRelatedCourses(userId, courseId)
        .then(res => {
            dispatch({ type: types.FETCH_RELATED_COURSES, payload: res.data });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}