import * as types from './types';
import AuthService from '../services/authServices';
import { setLocalStorageAuth } from '../utils/helpers';
import cookie from 'react-cookies';

export function login(username, password, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        AuthService.login(username, password)
            .then(data => {
                setLocalStorageAuth(data.data);
                dispatch({ type: types.LOGIN_SUCCESS, ...data.data });
                dispatch({ type: types.ALERT_CLEAR, isloading: false });
               
                if(data.data.userType === 1){
                    history.push('/user/course-details/MQ==');
                }else{
                    history.push('/user/course-info/MQ==');
                }
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.LOGIN_FAILED, ...errorData });
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
}

export function logout(history) {
    return dispatch => {
        AuthService.logout()
            .then(
                data => {
                    setLocalStorageAuth(false);
                    dispatch({ type: types.LOGOUT });
                    dispatch({ type: types.ALERT_SUCCESS, message: 'Logout Successfully' });
                    history.push('/login');
                }
            );
    }
}

export function subscriberLogin(username, password, history) {

    return dispatch => {
        dispatch({ type: types.ALERT_CLEAR, isloading: true });
        AuthService.subscriberLogin(username, password)
            .then(data => {
                setLocalStorageAuth(data.data);
                dispatch({ type: types.LOGIN_SUCCESS, ...data.data });
                dispatch({ type: types.ALERT_CLEAR, isloading: false });
                cookie.save('username', username, { path: '/login' });
                cookie.save('password', password, { path: '/login' });
                const currentDate = new Date(new Date().getTime() + 96 * 60 * 60 * 1000);
                cookie.save('auth', true, { path: '/' }, {expires: currentDate});
                history.push('/user-dashboard/me');
            })
            .catch(error => {
                if (error.response && error.response.status) {
                    let errorData = error.response.data;
                    dispatch({ type: types.LOGIN_FAILED, ...errorData });
                    dispatch({ type: types.ALERT_ERROR, ...errorData });
                } else {
                    dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
                }
            });
    };
    
}


