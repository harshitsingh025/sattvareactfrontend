import * as types from './types';
import VideoServices from '../services/videoServices';

export const fetchStyleVideos = (id,limit,from, videos) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    VideoServices.fetchStyleVideos(id,limit,from)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchNewVideos = (limit,from, videos) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    VideoServices.fetchNewVideos(limit,from)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchMyClassesVideos = (id,limit,from, videos) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    VideoServices.fetchMyClassesVideos(id,limit,from)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchMyFavoriteVideos = (id,limit,from, videos) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    VideoServices.fetchMyFavoriteVideos(id,limit,from)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchRecentWatchedVideos = (id,limit,from, videos) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    VideoServices.fetchRecentWatchedVideos(id,limit,from)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchRecommendedVideos = (limit,from, videos) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    VideoServices.fetchRecommendedVideos(limit,from)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}

export const fetchSearchVideos = (data, videos, states) => dispatch => {
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    dispatch({ type: types.VIDEO_FILTER, filter: states });
    VideoServices.fetchSearchVideos(data)
        .then(res => {
            let allVideos = [...videos, ...res.data.videos];
            dispatch({ type: types.FETCH_VIDEOS, cursor: res.data.cursor, hasMore: res.data.hasMore, videos: allVideos, totalCount: res.data.totalCount });
            dispatch({ type: types.ALERT_CLEAR, isloading: false });
        })
        .catch(error => {
            if (error.response && error.response.status) {
                let errorData = error.response.data;
                dispatch({ type: types.ALERT_ERROR, ...errorData });
            } else {
                dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            }
        });

}