import * as types from './types';

export const updateChat = (messages) => dispatch =>{
    console.log(messages);
    dispatch({ type: types.LIVE_CHAT_MESSAGES, payload: messages });
}

