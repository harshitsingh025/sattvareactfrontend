import * as types from './types';
import axios from 'axios';
import TeacherService from '../services/teacherServices';



export const fetchTeacher = () => dispatch =>{
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
	axios.get('https://betasite.online/sattva-courses/api/get-teachers-data')
    .then(res =>{
    	dispatch({type: types.FETCH_TEACHER, payload: res.data});
        dispatch({ type: types.ALERT_CLEAR, isloading: false });
    })
     .catch(error => {
        if (error.response && error.response.status) {
            let errorData = error.response.data;
            dispatch({ type: types.ALERT_ERROR, ...errorData });
        } else {
            dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
        }
    });

}

export const fetchTeacherDetails = (id, history) => dispatch =>{
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
	axios.get('https://betasite.online/sattva-courses/api/get-teacher-details/'+id)
    .then(res =>{
    	dispatch({type: types.FETCH_TEACHER_DETAILS,payload: res.data });
        dispatch({ type: types.ALERT_CLEAR, isloading: false });
    })
    .catch(error => {
        if (error.response && error.response.status) {
            let errorData = error.response.data;
            dispatch({ type: types.ALERT_ERROR, ...errorData });
            history.push('/teachers');
        } else {
            dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
            history.push('/teachers');
        }
    });

}

export const sendTeacherEmail = (data) => dispatch =>{
    dispatch({ type: types.ALERT_CLEAR, isloading: true });
    TeacherService.sendTeacherEmail(data)
    .then(res =>{
       dispatch({ type: types.ALERT_SUCCESS, message: 'Mail Sent Successfully' });
    })
    .catch(error => {
        if (error.response && error.response.status) {
            let errorData = error.response.data;
            dispatch({ type: types.ALERT_ERROR, ...errorData });
        } else {
            dispatch({ type: types.ALERT_ERROR, message: 'Something Went Wrong, Try Again' });
        }
    });
}

export const resetAlertValues = () => dispatch =>{
    dispatch({ type: types.ALERT_CLEAR, isloading: false });  
}