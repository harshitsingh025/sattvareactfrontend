import Constants from '../constants';

export function apiRoute($url) {
    return Constants.WEBSITE_URL + $url;
}

export function imagePath($image) {
    return Constants.IMAGE_PATH + $image;
}

export function teacherImagePath($image) {
    return Constants.TEACHER_IMAGE_PATH + $image;
}

export function courseImagePath($image) {
    return Constants.COURSE_IMAGE_PATH + $image;
}

export function teacherImageDetailsPath($image) {
    return Constants.TEACHER_IMAGE_DETAILS_PATH + $image;
}

export function teacherVideoDetailsPath($image) {
    return Constants.TEACHER_VIDEO_DETAILS_PATH + $image;
}

export function userProfilePath($image) {
    return Constants.USER_PROFILE_PATH + $image;
}

export function getApiHeader(extraHeader = {}, checkAuth = false) {
    let headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    };

    if (checkAuth) {
        let accessToken = getAuthorizationToken();
        
        if (accessToken) {
            headers.Authorization = 'Bearer ' + accessToken;
        }
    }

    return { ...headers, ...extraHeader };
}

export function getLocalStorageAuth() {
    let authData = localStorage.getItem('auth');
    
    if (authData) {
        return JSON.parse(authData);
    }

    return false;
}

export function getAuthorizationToken() {
    let authData = localStorage.getItem('auth');

    if (authData) {
        authData = JSON.parse(authData);
        return authData.access_token;
    }

    return false;
}

export function setLocalStorageAuth(authData) {
    if (authData) {
        localStorage.setItem('auth', JSON.stringify(authData));
        return true;
    } else {
        localStorage.removeItem('auth');
        return false;   
    }
}

export function removeLocalStorageAuth() {
    localStorage.removeItem('auth');
}

export function getUserId(history) {
   let authData = localStorage.getItem('auth');
    if (authData) {
        authData = JSON.parse(authData);
        let userId = authData.userDetails.id;
        if(userId){
           return userId;
         }else{
            localStorage.removeItem('auth');
            history.push('/')
         }
    }else{
        localStorage.removeItem('auth');
        history.push('/')
    }
    return false;
}
